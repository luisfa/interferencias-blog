# Sitio web de Interferencias
### https://interferencias.tech/

Asociación ciberactivista dedicado a organizar actividades y compartir información y opinión sobre derechos digitales, seguridad informática y temáticas similares, además de con el objetivo de promover el software libre.

## ¿Qué es Interferencias?
Interferencias es una asociación ciberactivista sin ánimo de lucro que pretende 
reunir a una serie de personas interesadas en:
* Derechos digitales
* Privacidad en Internet
* Vigilancia masiva
* Seguridad informática

La idea es compartir información sobre estos temas, al igual que hacer mesas 
redondas para conocer las opiniones del resto de personas y participar activamente 
en la defensa de los derechos en internet y la privacidad.

Este grupo se basa en los ideales de la [EFF](https://www.eff.org) sobre la 
importancia de que estos temas sean de conocimiento público y cercano para todas 
las personas, así como que se luche tanto desde las instituciones como desde las 
comunidades educativas,como hacen con [Electronic Frontier Alliance](https://www.eff.org/fight) 
para el activismo estudiantil.

## Interferencias en las redes sociales
Puedes seguirnos en:
* [Mastodon](https://mastodon.technology/@interferencias)
* [Telegram](https://t.me/inter_ferencias_ruido)
* [Twitter](https://twitter.com/Inter_ferencias)

## Contacto
Si necesitas más información o quieres comentarnos cualquier cuestión, puedes 
escribirnos por correo electrónico a [info@interferencias.tech](mailto:info@interferencias.tech)

También puedes unirte a nuestro grupo de Telegram, que es el medio de comunicación 
más directo que usamos: [https://t.me/inter_ferencias](https://t.me/inter_ferencias)

***

Web basado en la plantilla [**HPSTR Jekyll Theme**](https://github.com/mmistakes/hpstr-jekyll-theme) de [Michael Rose](https://github.com/mmistakes).
