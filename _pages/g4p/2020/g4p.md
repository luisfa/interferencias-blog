---
layout: page
title: GIRLS 4 PRIVACY
permalink: /g4p/
description: GIRLS 4 PRIVACY
image:
  feature: banners/header.jpg
timing: false
---

![g4p]({{ "/assets/images/g4p/2020/banner.jpg" }})

En **Interferencias** estamos comprometidas con las actividades que desarrollen la curiosidad y la evolución de diferentes colectivos por los derechos digitales, la privacidad y la tecnología. Creemos que aún estamos a tiempo de crear un futuro más justo en la red, de empoderar a los usuarios y potenciar la empatía digital. Pero no podemos hacerlo solas.

Desde que el grupo se creó en 2016, hemos estado detrás de charlas, eventos, hackathones, debates… Todo para **hacer ruido** y recordarle a los usuarios que tenemos derechos y debemos protegerlos. Poco a poco nos sumamos a más movimientos hermanos, y tenemos una firme postura en pro del software libre y el desarrollo digital. Hay un tema, dentro del contexto de la tecnología, que nos preocupa. Diversos estudios demuestran que hay un período clave entre la educación primaria y secundaria en la que las estudiantes (chicas) comienzan a perder interés en carreras técnicas por diversas razones, la mayoría estigmas culturales, pero también temor a un camino difícil, a una lucha constante por su valía como técnicas y a una considerable falta de modelos femeninos en la industria. Además el problema continúa, pues el porcentaje de abandono de las mujeres que deciden meterse en el mundo de la tecnología también es preocupante.

Hace unos meses le expusimos esta preocupación a [Kaspersky](https://www.kaspersky.es/), y decidieron ayudarnos. Queremos escuchar propuestas e ideas que nos ayuden a impulsar nuestros objetivos principales (la privacidad, los derechos digitales, la ciberseguridad….) y queremos hacerlo dandole voz a ese grupo de mujeres que están deseando aportar a la comunidad. Es por ello que, gracias al patrocinio de **Kaspersky Labs**, lanzamos una beca para sacar adelante un proyecto relacionado con los objetivos de Interferencias pero que esté compuesto en su mayoría (al menos del 60% del grupo) por mujeres. Además, la condición es que el grupo sea dirigido por una de éstas mujeres. Sabemos que habrá muy buenas ideas esperando a tener una oportunidad para desarrollarse, queremos formar parte de eso.

Lanzamos el concurso **Girls 4 Privacy**, una competición de proyectos con licencia libre en varias fases, con la idea de poder becar la propuesta ganadora. En esta primera edición los proyectos participantes son:

## Manual de buenas prácticas de software libre para la investigación social

<blockquote>
El desarrollo de las tecnologías de la información ha permeado las formas de trabajo, también en el ámbito académico. Los investigadores/as emplean diariamente una multitud de herramientas y servicios en línea, sin realizar siempre un análisis crítico de las consecuencias de su uso. En este proyecto planteamos la publicación de un manual divulgativo para que los académicos y académicas puedan acceder a él y cambiar sus hábitos en favor de las tecnologías libres.<br><br>
Este proyecto se vincula directamente a la convocatoria planteada por “Girls 4 privacy” en dos sentidos. Primeramente, la utilización de herramientas libres contribuye al respeto de la privacidad de los usuarios/as y de los datos gestionados en ellas. Y, en segundo lugar, la comunidad científica, especialmente en Ciencias Sociales, se encuentra en continuo contacto con las sociedades que investiga. Por ello, es una necesidad de primer interés conocer cómo proteger no solo la información de las personas y comunidades que forman parte del estudio, sino también de las comunicaciones que se tienen con ellas, así como con el resto de la comunidad universitaria (profesorado y alumnado).<br><br>
Pero, además, la utilización de software libre facilita la apertura del saber académico, el libre acceso a los métodos de investigación por parte de la ciudadanía. Y adicionalmente, las universidades, con un gran peso institucional, político y económico, resultan un apoyo relevante para el desarrollo y fortalecimiento de los proyectos de software libre existentes y el nacimiento de otros nuevos adaptados a necesidades concretas.
</blockquote>

### Equipo:
* **Dafne Calvo Miguel**
Investigadora predoctoral en comunicación digital en la Universidad de Valladolid. Su tesis doctoral trata sobre cultura y software libre. Ha participado en PucelaBits, ha contribuido a la traducción en castellano del proyecto Empoderamiento digital y ha desarrollado el mapa Resistencias digitales a través del plan de Ushahidi grassroots development.

* **Lorena Cano Orón**
Doctora en Comunicación e Interculturalidad por la Universidad de Valencia. Sus líneas de investigación comparten el análisis del flujo de contenidos en Internet; le interesa el ámbito de la privacidad digital desde un punto de vista sociológico: su percepción social y los efectos de la personalización del espacio público digital. En su proyecto postdoctoral trabaja la desinformación online.

## Keep It Safe: Save your accounts

<blockquote>
Extensión para navegadores web (como pueden ser Chrome, Firefox, etc.) el cual se encargaría de cerrar sesiones que se hayan dejado abiertas al salir de la última pestaña o enviar un recordatorio en el que figuren dichas sesiones. Se incluirían las páginas más comunes, es decir, redes sociales punteras y servicios de correo electrónico (Gmail, Hotmail, Facebook, Instagram...) con el fin de proteger y mejorar la privacidad del usuario.<br><br>
Estará disponible en al menos español e inglés para conseguir una mayor expansión del proyecto. Se puede aplicar al uso personal, teniendo un recordatorio de cerrar la sesión, o al uso en institutos, oficinas u ordenadores públicos de, por ejemplo, bibliotecas, haciendo que las sesiones se cierren automáticamente mediante un horario programable. Con este proyecto queremos facilitar la protección del usuario ante la distribución de cuentas personales y lograr así un grado más alto de privacidad digital.
</blockquote>

### Equipo:
* **Aurora Lima [portavoz], Aitana Bellveser, Daniela I. Vázquez, Sabino Marichal y Víctor Rodríguez**
Grupo de alumnas del instituto IES Garoé (El Hierro, Islas Canarias) que están cursando 1º y 2º de Bachillerato y participan en este concurso por la concienciación que tiene con la seguridad y el anonimato en la red.

## Feminatic/Fanzine. Fanzine sobre derechos digitales

<blockquote>
Feminatic/Fanzine es una serie de fanzines donde se abordarán temas vinculados a los derechos digitales, la privacidad, la ciberseguridad, la violencia digital y los autocuidados, todos ellos desarrollados y elaborados desde una perspectiva de género.<br><br>
Planteamos una primera etapa de cinco (5) temas: derechos digitales humanos, privacidad y violencias digitales, autocuidados en línea, ciberfeminismo y ciberactivismo, narrativas y noticias falsas sobre ciberseguridad.<br><br>
Relacionado a nuestro objetivo y público (jóvenes) el diseño de fanzines estará planteado como una primera y complementaria fase de un proceso donde pensamos a dicho recurso como material de difusión pero también educativo. Por tanto, pensamos en los fanzines como herramienta potencial de uso en talleres donde sensibilizar y promover la perspectiva de género y los derechos digitales.<br><br>
Promover la autonomía de jóvenes y adolescentes en el uso de las tecnologías digitales. Sensibilizar en temas vinculados a violencias machistas en los espacios digitales para mejorar sus sus prácticas de protección y seguridad digital desde una perspectiva de géneros y feminista.<br><br>
El recurso fanzine contiene narrativas de uso apropiados y es una interfaz colaborativa para trabajar con jóvenes y adolescentes en temas situados.
</blockquote>

### Equipo:
* **Ivana Mondelo**
Licenciada en Comunicación Social (UNR) especializada en Comunicación Digital. Ciberfeminista, interesada en el cruce entre tecnología y género, la cultura y los derechosdigitales. Ha trabajado en proyectos de participación e innovación ciudadana y en talleres de inclusión digital dirigido a jóvenes y adolescentes. Motivada en el universo digital porque abre la posilibilad de continuar aprendiendo y compartiendo saberes, generando y potenciando nuevas redes, vínculos y espacios de participación. 

* **Virginia Brussa**
Licenciada en Relaciones Internacionales (UNR), maestranda en Comunicación Digital Interactiva. Interesada en derechos digitales y procesos colaborativos. Ha trabajado en proyectos sobre imaginarios jóvenes argentinxs, políticas participativas y diseño de metodologías digitales. Motivada en compartir e intercambiar saberes con jóvenes y adolescentes para repensar derechos, riesgos y formas de hacer en lo digital.
