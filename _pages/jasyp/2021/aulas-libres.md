---
layout: page
title: Participaciones confirmadas para Aulas Libres (Fuera Google) en esLibre 2021
permalink: /eslibre/aulas-libres/
description: "Propuestas aceptadas para Derechos Digitales y Privacidad en Internet esLibre 2021"
image:
  feature: banners/header.jpg
timing: false
---

<img src="{{ site.url }}/assets/images/eslibre/2021/eslibre_2021_small.png" style="display: block; margin: 0 auto;">

<br>
<h2 style="text-align:center" id="demanda-convenios-google">"Demanda judicial contra los convenios con Google la necesidad de la acción colectiva"</h2>

**Luis Fajardo López**, Prof. de Derecho civil en la ULL. Defensor de las libertades en el ciberespacio. Dr. por la UAM, ha dado clases en la UAM, UAL, UdG, y UNED. Ha sido abogado, Juez, y DPD. Actualmente está centrado en su trabajo docente y de investigación en la ULL y en la constitución de la Fundación Tecnología para la Sociedad, txs.es.

* **Web del proyecto**: <https://www.educar.encanarias.info/fundacion/>
* **Web personal**: <https://encanarias.info/people/56066f30d1620135a1394b44294f2d2e>
* **Mastodon**: [@lfajardo@txs.es](https://txs.es/@lfajardo)
* **Twitter**: [@lfajardo](https://twitter.com/lfajardo)

<blockquote>
Al principio de la pandemia reaccioné como padabogado y DPD: el convenio con Google de mi comunidad autónoma es ilegal (como los demás), y serviría además de ariete para abrirles las puertas de otras. Pedí el expediente, lo recurrí ante la Administración, pero para la vía judicial no podía presentarlo solo... Te contaré los argumentos, lo que ha venido después preparando Tecnologías para la Sociedad, y cómo puedes colaborar para presentarla antes del comienzo de curso en toda España.<br><br>
Hace ya más de un año que nos vimos obligados, llevados por la pandemia, a hacer frente al desembarco masivo de tecnologías que invaden la privacidad de nuestros hogares. Como profesor monté un #Jitsi para dar mis clases, colaboré más que me enfrenté a las políticas de mi universidad, y junto a otros compañeros terminamos montando el embrión de #EDUCATIC, que hoy gestiona la plataforma de videoconferencias libre (BigBlueButton) con la que impartimos docencia, con interés de los compañeros (impartimos un curso a profesorado al que se inscribieron 179) y el beneplácito del Rectorado. Seguimos avanzando porque queda mucho por hacer.<br><br>
Finalmente constituimos la Asociación EDUCATIC, Tecnologías para la Educación y Transparencia Tecnológica, una de cuyas finalidades es dar vida a una fundación que garantice la continuidad del proyecto: txs.es, correo libre y seguro, redes sociales libres y seguras, herramientas para el trabajo que respeten nuestros valores (y nuestras leyes, claro), y cómo no, aplicaciones para la docencia. Queremos seguir trabajando para que los centros educativos en España utilicen tecnología ética, que enseñen a nuestra juventud el potencial de la tecnología, sin hacerlos usuarios cautivos de plataformas de captación de datos y consumo tecnológico.<br><br>
Ahí entran las acciones judiciales, que detallaré. Puedes ver más en <a href="https://encanarias.info/posts/19db429025a40139409b26860af9fe8c" target="_blank">esta entrada</a>.
</blockquote>

<br>
<h2 style="text-align:center" id="educamadrid-plataforma-educativa-libre">"EducaMadrid, la Plataforma Tecnológica Educativa y Libre de la Comunidad de Madrid"</h2>

**Adolfo Sanz de Diego** empezó su carrera profesional como desarrollador web con Java, hasta que en 2008 dió el salto a la docencia como profesor de FP de Informática. Ha trabajado como Asesor de Formación, como Asesor Técnico y actualmente como Jefe de Servicio de Plataformas Educativas en la Consejería de Educación de la Comunidad de Madrid. Compagina su trabajo principal colaborando como formador técnico, ponente en conferencias técnicas y profesor en la Universidad de Alcalá.

* **Web del proyecto**: <https://www.educa2.madrid.org/educamadrid/>
* **Web personal**: <https://www.asanzdiego.com/>
* **Twitter**: [@asanzdiego](https://twitter.com/asanzdiego)

<blockquote>
EducaMadrid es una Plataforma Tecnológica Educativa basada en Software Libre que ofrece múltiples servicios interconectados y complementarios: webs de los centro, blogs, aulas virtuales, mediateca, cloud, correo, videoconferencias, MAdrid_linuX, etc.<br><br>
Ofrece la posibilidad de desarrollar la competencia digital, la comunicación entre alumnado y docentes e implementar procesos de enseñanza aprendizaje tanto de manera presencial como a distancia en un entorno seguro, independiente y sostenible. Además, es un entorno sostenible, que no genera usuarios cautivos, que respeta la autoría de los contenidos generados por sus usuarios, y que facilita la movilidad entre centros a alumnado y docentes.<br><br>
Las Aulas Virtuales son, sin duda, el servicio estrella de EducaMadrid. Permiten al profesorado crear cursos online en donde el alumnado puede asistir a clases por videoconferencia, descargar documentos, visualizar vídeos, entregar ejercicios, recibir correcciones, comunicarse con sus compañeros y docentes por mensajería instantánea o por foros, e incluso hacer exámenes online desde cualquier parte usando teléfonos, tablets y/o ordenadores.<br><br>
</blockquote>

<br>
<h2 style="text-align:center" id="propuesta-fase-cgt">"En defensa de una educación digital pública: la propuesta de FASE CGT"</h2>

**Juan Miguel Mendoza Garrido**, profesor EESS y Secretario de Organización de FASE CGT.

* **Web de la iniciativa**: <https://edigpubfasecgt.org/>
* **Correo electrónico**: <sopri@edigpub.org>

<blockquote>
Ante el proceso acelerado de privatización de los recursos digitales de la educación pública en Andalucía, FASE CGT presenta a la comunidad educativa un manifiesto, abierto a ampliación, en el que se señalan los pilares fundamentales que creemos que deben sostener el entramado digital de la escuela pública, y por los que debe luchar toda la comunidad educativa en aras de garantizar su soberanía y privacidad digitales.<br><br>
La privatización avanza a golpe de convenios con multinacionales como Google o Microsoft, que con la excusa del “coste cero” pueden terminar haciéndose con el monopolio no solo de las plataformas digitales de la escuela pública, sino también con el control de sus contenidos educativos y de los datos personales de todos los miembros de la comunidad educativa.<br><br>
La supresión el IEDA (Instituto de Enseñanzas a Distancia de Andalucía) y la dispersión de sus enseñanzas y cometidos, es un ejemplo de esta política de desmantelar los recursos y medios públicos de la educación digital para en el corto plazo justificar la necesidad de su privatización.<br><br>
FASE CGT pone al servicio de la comunidad educativa una web en permanente actualización para difundir noticias y conocimiento que fomenten el uso de los recursos abiertos y de softward libre en los centros públicos, así como apoyo y asistencia a quienes se sientan compelidos a renunciar a lo público para someterse a la dictadura de las corporaciones.<br><br>
</blockquote>

<br>
<h2 style="text-align:center" id="digitalizacion-democratica-centros-educativos">"Excelencia en la privacidad de datos y la digitalización democrática de los centros educativos"</h2>

**Sergio Salgado** estudió Ciencias Políticas en la Universidade de Santiago de Compostela y Ecología Humana en la Universidade Aberta de Lisboa. Fundador de Pantheon Work, consultora especializada en la gestión de comunicación, organización y reputación. Asesora varias organizaciones e instituciones. Está Implicado en varios proyectos de activismo y es coautor junto con Simona Levi del Libro «Votar y cobrar – La impunidad como forma de gobierno» y de la obra teatral «Hazte Banquero».

* **Web del proyecto**: <https://xnet-x.net/privacidad-datos-digitalizacion-democratica-educacion-sin-google/>
* **Web personal**: <https://xnet-x.net/sergio-salgado/>
* **Twitter**: [@X_net_](https://twitter.com/X_net_)

<blockquote>
Propuesta de solución de Xnet y familias promotoras para la excelencia en la privacidad de datos y la digitalización democrática de los centros educativos.<br><br>
Con la intención de paliar el déficit existente sobre privacidad y soberanía de datos en los centros educativos proponemos un Plan holístico que nos facilitará acceder a una digitalización democrática ágil, efectiva y respetuosa de las libertades fundamentales.<br><br>
Objetivos específicos a alcanzar para resolver el problema global:<br><br>
1. Servidores seguros y respetuosos con los derechos humanos y acciones relacionadas<br><br>
2. Suite for Education: herramientas estructuralmente auditables que ya existen agregadas en una única suite<br><br>
3. Formación y capacitación digital de la comunidad educativa: task force para traspaso de las competencias y adecuación de los programas de formación digital<br><br>
4. Cuestiones transversales para ayudar el ecosistema empresarial, el correo y otras cuestiones relacionadas con la digitalización general<br><br>
5. Diseño de implementación y presupuesto de nuestro plan de digitalización democrática de los centros educativos<br><br>
6. Repositorio de información para argumentarios<br><br>
</blockquote>

<br>
<h2 style="text-align:center" id="familias-criticas-opensource">"Familias críticas por el Open Source"</h2>

El **GRITE** es un **Grupo de Reflexión y de Información sobre las Tecnologías Educativas** formado por miembros de la **AMPA del CEIP Gómez Moreno (Granada)** y abierto al conjunto de la comunidad educativa. Nuestra propuesta quiere activar la colaboración con el centro para argumentar y reflexionar en base a la realidad de nuestra comunidad educativa. Participarán en su representación Marion, Manu y Rakel.

* **Twitter**: [@ampagomezmoreno](https://twitter.com/ampagomezmoreno)
* **PeerTube**: <https://tubedu.org/video-channels/grite_gomez/videos>
* **Correo electrónico**: <grite_gomez@protonmail.com>

<blockquote>
Buscamos realizar un diagnóstico participativo. Para ello, necesitaríamos cierta información para trabajar juntas en el óptimo desarrollo crítico de las competencias digitales. Por eso es imprescindible cuantificar la situación, por ejemplo, conocer cuantas familias han dado su consentimiento sobre las políticas de tratamiento de datos de la plataforma Google for Education que se han empezado a usar masivamente en los centros educativos, así como investigar el estado del conocimiento digital de las familias (digital readiness) y, por tanto, el grado de consentimiento informado que se ha dado en los datos anteriormente solicitados.<br><br>
Hemos estado trabajando en el diseño de unos objetivos que motivan esta iniciativa y queremos compartirlo con vosotras y vosotros para trabajar toda la comunidad educativa alineada:<br><br>
- Divulgar y sensibilizar sobre el programa de Transformación Digital Educativa (TDE) de la Junta de Andalucía, acompañando el proceso de implementación desde una perspectiva que garantice la privacidad, intimidad y seguridad de lxs menores y promueva la autonomía, la autogestión y la soberanía digital sobre los datos. <br><br>
- Reflexionar sobre el uso de GAFAM en primaria y posibles alternativas.<br><br>
- Apoyar la utilización del software libre.<br><br>
- Privilegiar soluciones no tecnológicas y presenciales cuando es posible.<br><br>
- Formación multidisciplinar y transversal sobre el entorno digital.<br><br>
- Fomentar la capacidad de los jóvenes de utilizar las nuevas tecnologías de forma segura y responsable.<br><br>
- Que las nuevas tecnologías sean un instrumento de aprendizaje para el alumnado y que este no sea instrumento de esas nuevas tecnologías.<br><br>
- Promover el desarrollo sostenible (green IT) en el uso de las nuevas tecnologías: circuito corto, autogestión, reciclaje, soberanía.<br><br>
- Reflexionar sobre la brecha tecnológica y la brecha generacional.<br><br>
</blockquote>

<br>
<h2 style="text-align:center" id="hezkuntzan-librezale">"Hezkuntzan librezale"</h2>

**Hezkuntzan ere Librezale** fue creado en 2017 por padres, profesores, alumos y agentes de Euskal Herria, preocupados por la creciente presencia de grandes empresas privadas en los procesos de digitalización de nuestras escuelas. En la actualidad, formamos este grupo más de 200 personas de toda Euskal Herria, tanto de la red pública como de la concertada.

* **Web del colectivo**: <https://hezkuntza.librezale.eus/>

<blockquote>
Teniendo en cuenta la situación de la educación vasca, desde el grupo Hezkuntzan ere Librezale queremos presentar la siguiente declaración para expresar nuestras inquietudes:<br><br>
1. La cesión de los datos de nuestros alumnos a empresas privadas impide garantizar su privacidad. El hecho de que entidades públicas impulsen que nuestros alumnos se conviertan en productos tiene graves implicaciones.<br><br>
2. Invertir en dispositivos Chromebook que solo garantizan cinco años de vida es una irresponsabilidad, ya que la obsolescencia programada va en contra de la reutilización.<br><br>
3. Dejar la presencia del euskera en los dispositivos que utilizarán nuestros alumnos a merced de los intereses y directrices de las empresas privadas es una decisión contraria a nuestra lengua. Es más, en los dispositivos Chromebook, hoy en día, hay varios apartados que no están en euskera.<br><br>
4. Restringir una y otra vez al alumnado el uso de las mismas herramientas es reducir la educación digital. La competencia digital, según las leyes educativas de Europa y del País Vasco, es una competencia básica a alcanzar por el alumnado, y limitarles a estas herramientas no la garantiza en ningún caso.<br><br>
5. El riesgo de crear dependencia de estas herramientas en la comunidad educativa es un grave problema. Impulsados por las instituciones, estamos creando potenciales usuarios y clientes de productos de empresas privadas.
</blockquote>
