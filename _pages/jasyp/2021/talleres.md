---
layout: page
title: Propuestas de talleres Derechos Digitales y Privacidad en Internet esLibre 2021
permalink: /eslibre/talleres/
description: "Propuestas de talleres Derechos Digitales y Privacidad en Internet esLibre 2021"
image:
  feature: banners/header.jpg
timing: false
---

<div class="bootstrap">

  <div style="width: 100% !important; margin: auto;">
    <form id="create-talks" role="form" action="https://c4p.interferencias.tech/form/workshops-es" method="post">
      <div class="form-group"><span class="form-text text-muted"> <ul style="list-style-position: inside; padding-left: 0">
          <li>Los campos marcados como (*) son obligatorios</li>
          <li>Una vez que haya sido aceptada se publicará en la <a href="https://propuestas-eslibre.interferencias.tech/2021" target="_blank">página de propuestas</a></li>
          <li>Recuerda que para cualquier duda puedes escribirnos a <a href="mailto:info@interferencias.tech">info@interferencias.tech</a></li>
          </ul>
        </span></div>
      <div class="form-group"><strong><label for="title">Título (*):</label></strong>
        <div id="title-counter-tag" class="tag is-danger"><span id="title-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="title" type="text" maxlength="150" onkeyup="counterDisplay(this, 'title', true)"
          required="required" class="form-control">
      </div>
      <div class="form-group"><strong><label for="description">Descripción (*):</label></strong>
        <div id="description-counter-tag" class="tag is-danger"><span id="description-counter-display">0</span>&nbsp;&nbsp;/&nbsp;&nbsp;5000</div><textarea name="description" cols="40" rows="5" maxlength="5000"
          style="resize:none; overflow-y: scroll;" aria-describedby="description-help-block" onkeyup="counterDisplay(this, 'description', true)" required="required" class="form-control"></textarea><span id="description-help-block"
          class="form-text text-muted">Descripción algo más extensa
          sobre la temática y el contenido de la propuesta.</span>
      </div>
      <div class="form-group"><strong><label for="goals">Objetivos a cubrir en el taller (*):</label></strong>
        <div id="goals-counter-tag" class="tag is-danger"><span id="goals-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;500&nbsp;</div><textarea name="goals" cols="40" rows="5" maxlength="500" style="resize:none; overflow-y: scroll;"
          aria-describedby="goals-help-block" onkeyup="counterDisplay(this, 'goals', true)" required="required" class="form-control"></textarea><span id="goals-help-block" class="form-text text-muted">¿Qué te gustaría que las personas que asistan al
          taller salgan conociendo en mayor o menor medida?</span>
      </div>
      <div class="form-group"><strong><label for="project">Web del proyecto:</label></strong>
        <div id="project-counter-tag" class="tag is-success"><span id="project-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="project" type="url" maxlength="150" aria-describedby="project-help-block"
          onkeyup="counterDisplay(this, 'project', false)" class="form-control"><span id="project-help-block" class="form-text text-muted">Página con información sobre el proyecto del que se hablará en la propuesta.</span>
      </div>
      <div class="form-group"><strong><label for="target">Público objetivo (*):</label></strong>
        <div id="target-counter-tag" class="tag is-danger"><span id="target-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;500&nbsp;</div><textarea name="target" cols="40" rows="5" maxlength="500" style="resize:none; overflow-y: scroll;"
          aria-describedby="target-help-block" onkeyup="counterDisplay(this, 'target', true)" required="required" class="form-control"></textarea><span id="target-help-block" class="form-text text-muted">¿A quién va dirigida?</span>
      </div>
      <div class="form-group"><strong><label for="speakers">Ponente/s (*):</label></strong>
        <div id="speakers-counter-tag" class="tag is-danger"><span id="speakers-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;500&nbsp;</div><textarea name="speakers" cols="40" rows="5" maxlength="500"
          style="resize:none; overflow-y: scroll;" aria-describedby="speakers-help-block" onkeyup="counterDisplay(this, 'speakers', true)" required="required" class="form-control"></textarea><span id="speakers-help-block"
          class="form-text text-muted">Necesitaríamos algo de información general
          sobre la persona o personas que van a llevar a cabo la propuesta: intereses personales, experiencia en la materia, si has realizado actividades similares...</span>
      </div>
      <div class="form-group"><strong><label for="name">Nombre (*):</label></strong>
        <div id="name-counter-tag" class="tag is-danger"><span id="name-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="name" type="text" maxlength="150" onkeyup="counterDisplay(this, 'name', true)"
          required="required" class="form-control">
      </div>
      <div class="form-group"><strong><label for="email">Email (*):</label></strong>
        <div id="email-counter-tag" class="tag is-danger"><span id="email-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="email" type="email" maxlength="150" onkeyup="counterDisplay(this, 'email', true)"
          required="required" class="form-control">
      </div>
      <div class="form-group"><strong><label for="web">Web personal:</label></strong>
        <div id="web-counter-tag" class="tag is-success"><span id="web-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="web" type="url" maxlength="150" aria-describedby="web-help-block"
          onkeyup="counterDisplay(this, 'web', false)" class="form-control"><span id="web-help-block" class="form-text text-muted">Por si tienes una web personal que quieras compartir.</span>
      </div>
      <div class="form-group"><strong><label for="mastodon">Mastodon (u otras redes sociales libres):</label></strong>
        <div id="mastodon-counter-tag" class="tag is-success"><span id="mastodon-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="mastodon" type="url" maxlength="150" aria-describedby="mastodon-help-block"
          onkeyup="counterDisplay(this, 'mastodon', false)" class="form-control"><span id="mastodon-help-block" class="form-text text-muted">Cualquier perfil en una red social libre es válido, pero recuerda introducir la URL completa para conocer
          también la instancia en la que te encuentras para que podamos encontrarte.</span>
      </div>
      <div class="form-group"><strong><label for="twitter">Twitter:</label></strong>
        <div id="twitter-counter-tag" class="tag is-success"><span id="twitter-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="twitter" type="text" maxlength="16" pattern="^@?[A-Za-z0-9_]{1,15}$"
          onkeyup="counterDisplay(this, 'twitter', false)" class="form-control"><span id="twitter-help-block" class="form-text text-muted">Si quieres que te mencionemos en Twitter al crear la propuesta, pon tu nombre de usuario.</span>
      </div>
      <div class="form-group"><strong><label for="gitlab">GitLab:</label></strong>
        <div id="gitlab-counter-tag" class="tag is-success"><span id="gitlab-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="gitlab" type="text" maxlength="255" pattern="^@?[A-Za-z0-9_-]{2,255}$"
          aria-describedby="gitlab-help-block" onkeyup="counterDisplay(this, 'gitlab', false)" class="form-control"><span id="gitlab-help-block" class="form-text text-muted">Si tienes un perfil en GitLab, pon unicamente tu nombre de usuario y te
          mencionaremos al crear la propuesta (en caso contrario no te preocupes, no hay ningún problema porque te avisaremos por correo igualmente).</span>
      </div>
      <div class="form-group"><strong><label for="github">Portfolio o GitHub (u otros sitios de código colaborativo):</label></strong>
        <div id="github-counter-tag" class="tag is-success"><span id="github-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="github" type="url" maxlength="150" aria-describedby="github-help-block"
          onkeyup="counterDisplay(this, 'github', false)" class="form-control"><span id="github-help-block" class="form-text text-muted">Cualquier otro sitio de desarrollo colaborativo donde tuvieras un perfil y quisieras mostrar tus proyectos, o
          también cualquier web que uses como portfolio.</span>
      </div>
      <div class="form-group"><strong><label for="prerequisites_attendees">Prerrequisitos para los asistentes:</label></strong>
        <div id="prerequisites_attendees-counter-tag" class="tag is-success"><span id="prerequisites_attendees-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;500&nbsp;</div><textarea name="prerequisites_attendees" cols="40" rows="5"
          maxlength="500" style="resize:none; overflow-y: scroll;" aria-describedby="prerequisites_attendees-help-block" onkeyup="counterDisplay(this, 'prerequisites_attendees', false)" class="form-control"></textarea><span
          id="prerequisites_attendees-help-block" class="form-text text-muted">Descripción de conocimientos mínimos recomendados, así como hardware/software del que deberían disponer las personas asistentes a la actividad.</span>
      </div>
      <div class="form-group"><strong><label for="prerequisites_organization">Prerrequisitos para la organización:</label></strong>
        <div id="prerequisites_organization-counter-tag" class="tag is-success"><span id="prerequisites_organization-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;500&nbsp;</div><textarea name="prerequisites_organization" cols="40" rows="5"
          maxlength="500" style="resize:none; overflow-y: scroll;" aria-describedby="prerequisites_organization-help-block" onkeyup="counterDisplay(this, 'prerequisites_organization', false)" class="form-control"></textarea><span
          id="prerequisites_organization-help-block" class="form-text text-muted">Descripción de elementos que sería ideal que la organización pudiera proveer para la correcta realización de la actividad (ejemplo: disposición de máquinas virtuales
          conun determinado software instalado para las personas que asistan a la actividad). <br><br>La organización no garantiza que se puedan satisfacer las necesidades de las actividades propuestos.</span>
      </div>
      <div class="form-group"><strong><label for="length">Duración: (*)</label></strong>
        <div id="length-counter-tag" class="tag is-danger"><span id="length-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="length" type="text" maxlength="150" aria-describedby="length-help-block"
          onkeyup="counterDisplay(this, 'length', true)" required="required" class="form-control"><span id="length-help-block" class="form-text text-muted">Indicar la duración de la propuesta. Por ejemplo, desde dos horas hasta un día entero.</span>
      </div>
      <div class="form-group"><strong><label for="day">Día: (*)</label></strong>
        <div id="day-counter-tag" class="tag is-danger"><span id="day-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;150&nbsp;</div><input name="day" type="text" maxday="150" aria-describedby="day-help-block"
          onkeyup="counterDisplay(this, 'day', true)" required="required" class="form-control"><span id="day-help-block" class="form-text text-muted">Indicar si se prefiere que sea el primer día o el segundo.</span>
      </div>
      <div class="form-group"><strong><label for="comments">Comentarios:</label></strong>
        <div id="comments-counter-tag" class="tag is-success"><span id="comments-counter-display">&nbsp;0</span>&nbsp;&nbsp;/&nbsp;&nbsp;500&nbsp;</div><textarea name="comments" cols="40" rows="5" maxlength="500"
          style="resize:none; overflow-y: scroll;" aria-describedby="comments-help-block" onkeyup="counterDisplay(this, 'comments', false)" class="form-control"></textarea><span id="comments-help-block" class="form-text text-muted">Cualquier otro
          comentario relevante para la organización.</span>
      </div>
      <div class="form-group">
        <div>
          <div class="custom-controls-stacked">
            <div class="custom-control custom-checkbox"><input name="privacy_email" type="checkbox"><label for="privacy_email">&nbsp;No quiero que mi email de contacto sea publicado con la
                información de la propuesta</label></div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div>
          <div class="custom-control custom-checkbox"><input name="privacy_social" type="checkbox"><label for="privacy_social">&nbsp;No quiero que mis redes sociales sean
              publicadas con la información de la propuesta</label></div>
        </div>
      </div>
      <div class="form-group">
        <div>
          <div class="custom-control custom-checkbox"><input name="accept_coc" type="checkbox" required="required"><label for="accept_coc">&nbsp;Acepto seguir el <a href="https://eslib.re/conducta" target="_blank">código de conducta</a> de
              <strong>es<span style="color:red">Libre</span></strong> y solicitar a las personas
              asistentes su cumplimiento (*)</label></div>
        </div>
      </div>
      <div class="form-group">
        <div>
          <div class="custom-controls-stacked">
            <div class="custom-control custom-checkbox"><input name="accept_coordination" type="checkbox" required="required"><label for="accept_coordination">&nbsp;Acepto coordinarme con la
                organización de esLibre para la realización del taller (*)</label></div>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div>
          <div class="custom-controls-stacked">
            <div class="custom-control custom-checkbox"><input name="accept_attend" type="checkbox" required="required"><label for="accept_attend">&nbsp;Al menos una persona estará conectada el día programado para impartir el taller (*)</label></div>
          </div>
        </div>
      </div>
      <div class="form-group text-center"><button name="submit" type="submit" class="btn btn-primary">Enviar</button></div>
    </form>
  </div>
</div>

<script src="/assets/js/jquery/dist/jquery.min.js"></script>
<script src="/assets/js/validation.js"></script>
