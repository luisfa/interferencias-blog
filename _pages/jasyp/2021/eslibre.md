---
layout: page
title: Sala esLibre 2021&#58 Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google)
permalink: /eslibre/
description: "Propuesta Sala Derechos Digitales y Privacidad en Internet + Soberanía Digital en las Aulas (Fuera Google) en esLibre 2021"
image:
  feature: banners/header.jpg
timing: false
---

<img src="{{ site.url }}/assets/images/eslibre/2021/eslibre_2021_small.png" style="display: block; margin: 0 auto;">

Se ha demostrado en los últimos meses que la colaboración, la autogestión y las comunidades constructivas son la clave para hacer funcionar el mundo frente a toda corriente. En esta situación, el software y el hardware libre tienen un hueco especial, conformado por una comunidad multidisciplinar dispuesta a colaborar por avance tecnológico responsable. **La comunidad de software/hardware libre ahora más que nunca debe estar dispuesta a crear herramientas prácticas que ayuden a la sociedad a avanzar de manera transparente y segura.**

<figure>
 <img src="{{ "/assets/images/eslibre/2021/grupo_14.jpg" }}" width="500" alt="Clausura esLibre 2019" style="display: block; margin: 0 auto;">
 <figcaption style="text-align: center; padding-top: 0em"><strong>Clausura esLibre 2019</strong></figcaption>
</figure>

Desde **Interferencias apoyamos firmemente el software libre**, creemos necesaria una tecnología responsable para incidir en varios elementos clave de la sociedad, además de la transparencia, la privacidad y los derechos digitales como base de cualquier proyecto. Uno de nuestros focos principales está en la educación, cuyas competencias tecnológicas se han acelerado en los últimos meses hacia recursos que poco cuidan la transparencia de su metodología. Romper la cadena de las herramientas privativas es una de las tareas más complicadas, pues se aprovechan de las dinámicas rápidas y fugaces en las que vivimos para obviar los muchos problemas que esconde el uso indebido de datos. Pero tenemos esperanza.

<figure>
 <img src="{{ "/assets/images/jasyp/20/mosaico_jasyp_2019_small.png" }}" alt="Mosaico JASYP '19" style="width:100%">
 <figcaption style="text-align: center; padding-top: 0em"><strong>JASYP '19</strong></figcaption>
</figure>

Desde 2017, Interferencias realiza unas [Jornadas de Anonimato, Seguridad y Privacidad (JASYP)](/jasyp) con la intención de acoger ideas diversas desde distintas áreas sobre estas temáticas, pero desde el pasado 2020 y mientras dure la situación sanitaria actual [nos acogemos a esLibre para ofrecer este espacio de intercambio de ideas](/eslibre2020). Creemos que aunque la experiencia no es igual a la de las jornadas originales, el evento nos brinda un entorno seguro, abierto e inclusivo acorde con nuestros ideales.

<figure>
 <img src="{{ "/assets/images/jasyp/18/charlas/mosaico_v.jpg" }}" alt="Mosaico JASYP '18" style="width:100%">
 <figcaption style="text-align: center; padding-top: 0em"><strong>JASYP '18</strong></figcaption>
</figure>

Nos gustaría poder acoger proyectos de todo tipo que apoyen la tecnología segura, que apoye la privacidad de los usuarios, sea transparente y abierta. Sería estupendo contar con voces diversas, que nos cuenten cómo apoyan las mismas cosas que nosotras, y que nos brinden una oportunidad de aprender desde perspectivas distintas. **Aceptamos charlas, talleres, propuestas y toda la clase de ideas que nos ayuden a crear un diálogo constructivo sobre la importancia y la necesidad de la defensa de los derechos digitales**.

<h2 style="text-align:center">Cómo enviar tu propuesta</h2>

<a href="https://eslib.re/2021/" target="_blank"><strong>es<span style="color:red">Libre</span></strong></a> es un encuentro de personas interesadas en las tecnologías y cultura libres, enfocado a compartir conocimiento y experiencia alrededor de las mismas. Al igual que en las ediciones anteriores, estamos ayudando con la organización de la propuesta presentada este año por <a href="https://librelabucm.org/" target="_blank">LibreLabUCM</a>.

El envío de propuestas está abierta a todo el mundo y usaremos un procedimiento de envío de propuestas similar al de esLibre, por eso, puedes usar los siguientes formularios para ello:

- [Propuestas de charlas](/eslibre/charlas/)
- [Propuestas de talleres](/eslibre/talleres/)
- [Otro tipo de propuestas](/eslibre/otras/)

Puedes ver todas las propuestas que se vayan presentando en <a href="https://gitlab.com/interferencias/propuestas/-/merge_requests" target="_blank">GitLab</a>, pero lo importante es que una vez que sean aceptadas pasarán a ser publicadas en <a href="/eslibre/aceptadas/" target="_blank">esta página</a>.

Si tienes cualquier duda, solo tienes que escribirnos a <info@interferencias.tech>.

<h2 style="text-align:center">Código de Conducta</h2>

<strong>Apoyamos el código de conducta de es<span style="color:red">Libre</span> que puedes leer <a href="https://eslib.re/conducta" target="_blank">aquí</a></strong>. Si hubiera un problema y necesitases ayuda, dirígete a cualquier miembro de la organización. También tendrás un contacto de Telegram a tu disposición, que estará debidamente indicado y bien visible durante la conferencia.
