---
layout: page
title: Propuestas aceptadas para Derechos Digitales y Privacidad en Internet esLibre 2021
permalink: /eslibre/aceptadas/
description: "Propuestas aceptadas para Derechos Digitales y Privacidad en Internet esLibre 2021"
image:
  feature: banners/header.jpg
timing: false
---

<img src="{{ site.url }}/assets/images/eslibre/2021/eslibre_2021_small.png" style="display: block; margin: 0 auto;">

<br>
<h2 style="text-align:center" id="libertar-para-compartir">"Libertad para compartir"</h2>

**Dario Castañé** es ingenierio informático, activista político y contribuidor en software libre. Actualmente dedica su (escaso) tiempo libre a proveer de herramientas de voto a organizaciones sin ánimo de lucro, mantener [Mergo](https://github.com/imdario/mergo) (una librería que vive en el corazón del cloud, usada por Docker, Kubernetes, etc.) desarrollada en Go y buscar nuevos campos en los que trabajar por un impacto social positivo usando la tecnología.

* **Web del proyecto**: <https://freesharing.eu/es>
* **Web personal**: <https://da.rio.hn>
* **Mastodon**: [@dario@mastodon.social](https://mastodon.social/@dario)
* **Twitter**: [@darccio](https://twitter.com/darccio)

<blockquote>
La Iniciativa Ciudadana Europea Freedom to Share aboga por la adopción de una disposición legislativa que contemple una exención de los derechos de autor, derechos afines y derechos sui generis del fabricante de bases de datos para las personas físicas que comparten archivos a través de redes digitales para fines personales y sin ánimo de lucro.
</blockquote>

<br>
<h2 style="text-align:center" id="politizar-la-tecnologia">"Politizar la tecnología: radios comunitarias y derecho a la comunicación en los territorios digitales"</h2>

**Inés Binder** es comunicadora social. Investiga temas de feminismo, tecnologías y políticas de comunicación. Es cofundadora del [Centro de Producciones Radiofónicas (CPR)](https://cpr.org.ar/) y parte del espacio hackfeminista [la_bekka](https://labekka.red/).

**Santiago García Gago** es comunicador social. Investiga temas de tecnopolítica y medios comunitarios. Es parte de [RadiosLibres.net](https://radioslibres.net/), [Radialistas.net](https://radialistas.net/) y de la [Red de Radios Comunitarias y Software Libre](https://liberaturadio.org/).

* **Web del proyecto**: <https://radioslibres.net/politizar-la-tecnologia/>
* **Mastodon**: [@radioslibres@mastodon.social](https://mastodon.social/@radioslibres)
* **Twitter**: [@RadiosLibres](https://twitter.com/radioslibres)

<blockquote>
Hoy en día la defensa del derecho a la comunicación exige, necesariamente, la apuesta por la soberanía tecnológica. Los medios libres, alternativos y comunitarios tienen experiencia en subvertir una tecnología unidereccional como la radio para garantizar la participación de las audiencias. En esta charla nos preguntaremos cuál es el nuevo horizonte tecnológico en la defensa del derecho a la comunicación desde los medios libres y esbozaremos algunas respuestas.<br><br>

Las radios comunitarias siempre han acompañado la defensa de estos territorios y de sus bienes comunes. Desde sus micrófonos se desenmascararon las falsas promesas de las mineras y se denunció cómo su explotación contaminaba los ríos. Apoyaron la organización vecinal que luchaba por una vida digna en los barrios periféricos de las grandes ciudades. Amplificaron los reclamos de los campesinos frente al ecocidio de los cultivos extensivos de palma africana. Y reclamaron un espectro radioeléctrico más diverso que reflejara la realidad y la cultura de sus comunidades.<br><br>

Hoy, estos territorios continúan siendo saqueados. Pero el asedio y la amenaza se extienden a los nuevos territorios digitales que habitamos. Entre las fronteras de Internet también existen fuerzas que colonizan y mercantilizan todos los aspectos de nuestra vida. Y de la misma manera, la ciudadanía resiste y se organiza alrededor de iniciativas que construyen autonomía y soberanía tecnológica.<br><br>

En esta charla plantearemos cómo podemos entender las iniciativas de soberanía tecnológica desde el marco de la defensa del derecho a la comunicación, con el que vienen trabajando los medios libres y comunitarios desde hace décadas.
</blockquote>

<br>
<h2 style="text-align:center" id="redes-sociales-transicion-social">"Redes sociales para una transición social"</h2>

**Niko** es admin de los servicios de <a href="https://www.nogafam.es/">nogafam.es</a>. Empezó siendo desarrollador de software y terminó en el lado oscuro (ingeniero de sistemas) de una empresa de Bilbao dedicada al open source. En <a href="https://blog.nogafam.es/blog/como-surgio-la-idea-del-proyecto-nogafam" target="_blank">este artículo</a> da un mayor detalle de cómo surgió el proyecto y su propuesta para solucionar el problema. Ha realizado algunos podcasts en colaboración de otra gente del Fediverso y también ha formado parte de charlas online sobre la soberania digital.

* **Web del proyecto**: <https://www.nogafam.es/>
* **Mastodon**: [@admin@masto.nogafam.es](https://masto.nogafam.es/@admin)

<blockquote>
Las redes sociales libres y federadas (el Fediverso) tienen un potencial increible que se debe dar a conocer a las personas. Me encantaría poder hablar de eso!<br><br>

Si bien el software libre no está adoptado de forma masiva, hay un campo en el que me atrevería a decir que no se usa ni un 5%: las redes sociales.<br><br>

El Fediverso es un mundo de redes sociales federadas que buscan un estandar en este campo, y viene para quedarse como el e-mail. Es solo cuestión de tiempo, de conocimiento y de mejora que esto acabe siendo así.
</blockquote>
