---
layout: page
title: JASYP '20 (Jornadas de Anonimato, Seguridad y Privacidad)
permalink: /jasyp/
description: "Formulario de participación para las JASYP '20"
image:
  feature: banners/header.jpg
timing: false
---

<h1 style="text-align:center; color:red;">Aplazamiento indefinido</h1>

**_Lamentamos tener que comunicar que hemos decidido aplazar de forma indefinida las JASYP '20 que estaban planificadas para los días 17 y 18 de abril, dado que por la situación actual, lo más correcto es no continuar con la organización de las mismas mientras que no se puede asegurar que no existe ningún riesgo para la salud pública por el COVID-19._**

**_La intención es que las Jornadas se realicen en algún momento de los próximos meses, pero es algo que todavía no podemos concretar. Os informaremos cuando tengamos más noticias._**

**_Sentimos mucho los inconvenientes generados._**

<hr>

**En Interferencias creemos que no hay nada más importante que las comunidades y las personas que las hacen posibles**, por eso siempre hemos querido contribuir intentando ayudar a crear espacios donde las personas puedan expresar libremente sus inquietudes en materia de **derechos digitales** y **seguridad informática**. Aunque la tecnología puede ayudarnos en la comunicación de forma muy simple (algo que de otra forma, quizás sería imposible), también nos gusta no perder la cercanía y la espontaneidad del intercambio de conocimientos, opiniones e ideas en persona.

![logo]({{ "/assets/images/jasyp/20/logo.png" }})

Ese motivo es que nos llevó a empezar a planear las **JASYP** (_Jornadas sobre Anonimato, Seguridad y Privacidad_), unas jornadas que llevamos organizando ya varios años ([2017]({{ site.url }}/jasyp/17/), [2018]({{ site.url }}/jasyp/18/), [2019]({{ site.url }}/jasyp/19/)) y donde se han tratado todo tipo de temas relacionados: vigilancia masiva, anonimato, criptografía, hacktivismo, antropología social, software libre, censura, criptomonedas, domótica, reconocimiento facial, sistemas biométricos, voto electrónico...

Por eso, otro año más nos hemos puesto manos a la obra con la organización de la edición del año 2020. A lo largo de estos años hemos conocido a un montón de gente de grupos como [BitUp Alicante](https://bitupalicante.com/), [Follow the White Rabbit](https://fwhibbit.es/), [Hack&Beers](https://hackandbeers.es/) (que todos los años ha servido como fiesta de presentación previa a las Jornadas), [Hacking Desde Cero](https://hackingd0.blogspot.com/) o [HackMadrid%27](http://hackmadrid.org/), pero también ha participado un gran número de personas independiente que tenían cosas muy interesantes que contar.

Todo esto es lo que nos gustaría que siguiera aumentando para llegar a tanta gente como sea posible; y claro está, aprovechar también las oportunidades que nos da nuestra tierra para compartir buenos ratos visitando nuestros bares o paseando por nuestros monumentos.

![logo]({{ "/assets/images/jasyp/20/cervezas.jpg" }})

La **edición de este año** (la cuarta ya) tendrá lugar **los días VIERNES 17 Y SÁBADO 18 DE ABRIL** en la **[Escuela Técnica Superior de Ingenierías Informática y de Telecomunicación de la Universidad de Granada](https://etsiit.ugr.es/)** _(Calle Periodista Daniel Saucedo Aranda, s/n, 18071 Granada)_. Según vayamos confirmando las distintas actividades, os iremos informando de las mismas por nuestro grupo de [Telegram](https://t.me/inter_ferencias) y nuestras redes sociales ([Mastodon](https://mastodon.technology/@interferencias) / [Twitter](https://twitter.com/inter_ferencias)).

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>Call for Ruido</h3>
		<hr>
  </div>
</div>

Todos los años tenemos la suerte de que personas de diferentes edades, sexos y grados de experiencia nos mandan una gran cantidad de propuestas muy diversas, tanto de perfiles tecnológicos como de perfiles más sociales. En cualquier caso, aquí indicamos de forma resumida qué tipos de participaciones nos gustaría recibir (puedes encontrar [el formulario](#form) al final de esta misma página):

- Personas interesadas en hablar sobre la importancia de los **derechos digitales**, la **privacidad en Internet**, la **seguridad informática** y todos aquellos temas de este ámbito que puedan tener relación.
- Las charlas pueden ser de cualquier área. Es cierto que la temática se presta a talleres y charlas técnicas, pero nos encantaría acoger también charlas relacionadas con la temática desde un punto de vista distinto, como puede ser el de personas dedicadas a **derecho, filosofía, política, arte** o cualquier otra rama. ¡Lánzate y cuéntanoslo!
- Si la charla está apoyada en **Software Libre** o cualquier contenido que sea libre, muchísimo mejor.
- Como complemento a la actividad principal, prepararemos otra edición del **Capture The Flag** donde cualquiera que se anime pueda poner a prueba sus habilidades a la hora de explotar las debilidades de sistemas informáticos. Las bases de participación serán publicadas próximamente, pero puedes ver algunos de los [writeups del CTF del año pasado](https://github.com/1r0dm480/CTF-Wr1T3uPs/tree/master/JASYPCTF19) hechos por nuestro amigo [1v4n](https://twitter.com/1r0Dm48O) de [Ηａϲｋеｒｓ４Ϝｕｎ ᏟᎢＦ Τеａⅿ](https://twitter.com/H4ck3rs4FunCTF)
- Está por confirmar si volveremos a hacer otra edición del [concurso de arte]({{ site.url }}/jasyp/arte/).

![portada]({{ "/assets/images/jasyp/20/portada_1.jpg" }})

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>¿Qué ofrecemos?</h3>
		<hr>
  </div>
</div>

En Interferencias somos un grupo de personas con mucha energía e interés, pero por desgracia no contamos con la suficiente solvencia económica como para poder hacernos cargo de los gastos de viaje y alojamiento para los participantes que tengan que desplazarse. Sin embargo, podemos ofreceros cerveza y tapas gratis, además de mucho buen humor granadino.

Es por eso que también estamos buscando patrocinadores que nos ayuden a cubrir estos gastos que nos gustaría poder afrontar de cara a hacer un evento de una mayor magnitud para la comunidad. Puedes encontrar el dossier de patrocinio de este año [aquí](/assets/pdfs/CALL4PATRONS_JASYP2020.pdf), pero se necesitas más información también puedes escribirnos a [jasyp@interferencias.tech](mailto:[jasyp@interferencias.tech]).

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>¿Te hemos convencido?</h3>
		<hr>
  </div>
</div>

En el caso de sea así, si todo esto te suena bien, aquí te dejamos el formulario a rellenar (**ABIERTO HASTA EL 9 DE MARZO**); aunque también agradecemos enormemente que nos ayudes con la difusión del evento a través de redes sociales o el simple boca a boca. Aceptamos propuestas de participantes de todas las personas sea cual sea tu edad, género, etnia o condición social, **seas quien seas, si te gusta la temática ¡ANÍMATE!**

![mosaico_2019]({{ "/assets/images/jasyp/20/mosaico_jasyp_2019_v2.jpg" }})
