---
layout: page
title: JASYP '19 (Jornadas de Anonimato, Seguridad y Privacidad)
permalink: /jasyp/19/
description: JASYP '19 (Jornadas de Anonimato, Seguridad y Privacidad)
image:
  feature: banners/header.jpg
timing: false
---

![cartel_jasyp]({{ "/assets/images/jasyp/19/cartel_jasyp_19_final_small.png" }})

Las **JASYP** (_Jornadas sobre Anonimato, Seguridad y Privacidad_) son unas jornadas que, desde hace ya unos cuantos años, son organizadas por [Interferencias](https://twitter.com/Inter_ferencias) con el apoyo de integrantes de otros grupos del ámbito de la seguridad informática como [Hacking Desde Cero](https://www.hackingdesdecero.org/), [Hack&Beers](https://twitter.com/hackandbeers), [BitUp Alicante](https://twitter.com/bitupalicante/), [HackMadrid%27](http://hackmadrid.org/) o [Follow the White Rabbit](https://twitter.com/fwhibbit_blog), con el objetivo de dar un espacio a todas aquellas personas con inquietudes sobre los problemas que encontramos en el día a día cada vez que usamos Internet o cualquier dispositivo electrónico en general.

En este 2019 lanzamos la tercera edición de este evento, donde esperamos poder aprender todavía más que otros años, aprovechar los días al máximo, y sacar partido del buen rollo que Granada siempre brinda en sus bares. La **edición de este año** tendrá lugar **los días VIERNES 26 Y SÁBADO 27 DE ABRIL** en la **[Escuela Técnica Superior de Ingenierías Informática y de Telecomunicación de la Universidad de Granada](https://etsiit.ugr.es/)** _(Calle Periodista Daniel Saucedo Aranda, s/n, 18071 Granada)_; pero la cosa no termina ahí porque seguramente tengamos un par de sorpresas que os iremos desvelando según se vayan acercando las fechas.

Por nuestra parte, lo único que podemos decir es que estamos muy felices ya que la respuesta ha sido muy buena en cuanto a participaciones recibidas, siendo además el año que hemos consiguiendo más variedad de participaciones entre temáticas técnicas y no técnicas, lo que creemos que puede ayudar a llegar a todo tipo de público. Por ello solo nos queda más que daros las gracias.

![uno]({{ "/assets/images/jasyp/19/actividades/uno.jpg" }})

<div class="bootstrap">
  <div class="text-center">
  <br>
  <h3>Actividades</h3>
  <hr>
</div>
</div>

Las JASYP de este año están enfocadas en 3 bloques:

- **Charlas y talleres**. Es el bloque principal de las Jornadas:
    + <a href="#programa">Programa</a>
    + <a href="#horario">Horario</a>
    + <a href="#hb">Hack & Beers</a>
- **Capture The Flag**. Segunda edición del CTF, organizada de nuevo en colaboración con nuestros amigos de [**Hacking Desde Cero**](http://www.hackingdesdecero.org/). Dentro de poco os iremos informando sobre como participar para poner a pruebas vuestras habilidades en el CTF Jeopardy-style que se está preparando.
- **Concurso de arte**. Y por último, también repetimos el concurso de arte. Consideramos que es importante hacer activismo en todas sus vertientes, por eso queríamos volver a organizar un concurso artístico como el [del año pasado]({{ site.url }}/jasyp/arte/). Si tienes cualquier duda puedes escribirnos a [jasyp@interferencias.tech](mailto:jasyp@interferencias.tech).

![dos]({{ "/assets/images/jasyp/19/actividades/dos.jpg" }})

<div class="bootstrap">
  <div class="text-center">
    <br>
    <h3 id="programa">Programa</h3>
  </div>
  </div>

  <div class="bootstrap">
    <div class="text-center">
      <hr>
      <table class="table table-bordered table-striped table-hover table-condensed table-responsive">
        <thead>
          <tr>
            <th class="not_mapped_style" style="text-align:center">

            </th>
            <th class="not_mapped_style" style="text-align:center">
              PONENTE
            </th>
            <th class="not_mapped_style" style="text-align:center">
              TÍTULO
            </th>
          </tr>
        </thead>
        <tbody>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/ale_cortes.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker1">Ale Cortés<br>
                (<a href="https://twitter.com/Kirzahk" target="_blank">@Kirzahk</a>)
                <div class="bio1">
                  <b>Bio:</b>
                  <ul>
                    <li>Perito Judicial Informático</li>
                    <li>Ingeniero Eléctrico</li>
                    <li>Profesor de Tecnología e Informática</li>
                    <li>Miembro de la <a href="http://www.brigadaantiacoso.com/" target="_blank">Brigada Antiacoso</a></li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Todas las disciplinas que forman parte de la ciberseguridad, prestando especial atención a los niños y jóvenes, por su especial desconocimiento en su empleo, de las leyes y todo lo relativo a su privacidad.</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act1n">Tecnoadicción, la droga del siglo XXI
                <div class="act1d">
                  <b>Descripción:</b><br><br>
                  La tecnología ha venido para quedarse, pero no nos damos cuenta que se está empleando desde edades cada vez más tempranas.<br><br>
                  Tras más de 30 charlas por el territorio andaluz y en continuo contacto con compañeros de todas las provincias de España y otros países sudamericanos, se aprecia que existe una peligrosa adicción a la tecnología, a estar todo el tiempo conectados, a los contenidos de Internet, independientemente de la edad, sexo, condición o situación económica. <br><br>
                  Es un estudio de primera mano con datos reales para presentar el panorama actual, a qué nos enfrentamos, pero también está orientada a que participen los asistentes, den sus opiniones, y si es posible, puedan plantearse algunas de las soluciones que se proponen.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/alex_garcia.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker2">Alex García<br>
                (<a href="https://twitter.com/Alexblackletter" target="_blank">@Alexblackletter</a>)
                <div class="bio2">
                  <b>Bio:</b>
                  <ul>
                    <li>Ingeniero de telecomunicaciones</li>
                    <li>Analista de protocolos en <a href="https://excemtechnologies.com/" target="_blank">Excem Technologies</a></li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act2n">Juego de Trazas
                <div class="act2d">
                  <b>Descripción:</b><br><br>
                  ¿Qué podemos saber acerca de tus paquetes de red?<br><br>
                  En esta charla se pretende mostrar la información que se puede obtener a partir de un cierto tráfico. También se enseñará cómo obtener dicha información.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/antonio_gamiz.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker3">Antonio Gámiz<br>
                (<a href="https://twitter.com/Antonio_D_Luffy" target="_blank">@Antonio_D_Luffy</a>)
                <div class="bio3">
                  <b>Bio:</b>
                  <ul>
                    <li>Estudiante de Doble Grado en Ingeniería Informática y Matemáticas</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act3n">Open Authentication (OAuth) 2.0
                <div class="act3d">
                  <b>Descripción:</b><br><br>
                  Charla explicatoria sobre cómo funciona <a href="https://es.wikipedia.org/wiki/OAuth" target="_blank">OAuth 2.0</a>, los tipos de grants y cómo los usan GitHub, Facebook y demases aplicaciones para dar servicio a terceros; además de cómo es posible atacarlo.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/bart_ortiz.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker4">Bart Ortiz<br>
                (<a href="https://twitter.com/bortizmath" target="_blank">@bortizmath</a>)
                <div class="bio4">
                  <b>Bio:</b>
                  <ul>
                    <li>Matemático interesado en usar las matemáticas para transformar la sociedad</li>
                    <li>Actualmente estudio el doctorado en Informática donde investigo en la representación y estudio de sistemas complejos desde el punto de vista computacional</li>
                    <li>Colaboro con varias asociaciones científicas y aprovecho cualquier oportunidad para divulgar ciencia</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Privacidad</li>
                    <li>Derechos digitales</li>
                    <li>Censura en Internet</li>
                    <li>Criptografía</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act4n">Hacking AI: Inteligencia Artificial y Seguridad
                <div class="act4d">
                  <b>Descripción:</b><br><br>
                  La inteligencia artificial nos rodea, cada día más y más aplicaciones descubren el valor de utilizar esta herramienta computacional para "ayudarnos" en nuestro día a día. Esto conlleva riesgos.<br><br>
                  El auge de estas tecnologías y su importancia está llevando a hackers de todo el mundo a investigar y alertar sobre las formas de manipulación y corrupción a las que pueden verse sometidas. Y el primer paso para detener esta amenaza es claro: ser conscientes de que
                  existe.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/celia_pedregosa.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker5">Celia Pedregosa (AIKUME)<br>
                (<a href="https://twitter.com/AilekUmbreon" target="_blank">@AilekUmbreon</a>)
                <!--
              <div class="bio5">
                <b>Bio:</b>
                <ul>
                  <li></li>
                </ul>
                <b>Intereses:</b>
                <ul>
                  <li></li>
                </ul>
              </div>
              -->
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act5n">Animales en la informática
                <div class="act5d">
                  <b>Descripción:</b><br><br>
                  Los animales son geniales los mires por donde los mires y es totalmente comprensible que la informática se haya fijado en ellos a lo largo de los años para llegar a ser lo que es ahora.<br><br>
                  [Charla apta para curiosos] «__»
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/ernesto_serrano.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker6">Ernesto Serrano<br>
                (<a href="https://twitter.com/erseco" target="_blank">@erseco</a>)
                <div class="bio6">
                  <b>Bio:</b>
                  <ul>
                    <li>DevOps Engineer en <a href="https://www.openexo.com/" target="_blank">OpenExO</a></li>
                    <li>Desarrollador principal del primer simulador de Stevie Wonder Simulator para Android</li>
                    <li>Rompiendo cosas desde el 83</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act6n">Sorry, but your common sense is in another castle: seguridad informática for dummies
                <div class="act6d">
                  <b>Descripción:</b><br><br>
                  Pequeña introducción a la seguridad informática, orientada a todos los públicos más para concienciar de los problemas que profundizar.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/fco_javier_lobillo.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker7">Fco. Javier Lobillo<br>
                (<a href="https://twitter.com/javierlobillo" target="_blank">@javierlobillo</a>)
                <div class="bio7">
                  <b>Bio:</b>
                  <ul>
                    <li>Doctor en Matemáticas</li>
                    <li>Profesor Titular en el <a href="http://algebra.ugr.es/" target="_blank">Departamento de Álgebra de la UGR</a> desde 2001</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Código correctores de errores</li>
                    <li>Criptografía</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act7n">Criptografía post-cuántica
                <div class="act7d">
                  <b>Descripción:</b><br><br>
                  Desde la publicación del <a href="https://es.wikipedia.org/wiki/Algoritmo_de_Shor" target="_blank">algoritmo de Schor</a>, todos los criptosistemas de clave pública usados en la actualidad deben ser considerados vulnerables bajo un ordenador cuántico, es decir, un ordanador cuántico sería capaz de calcular la
                  clave privada en tiempo polinomial.<br><br>
                  Por este motivo en los últimos años se ha incrementado notablemente la investigación dedicada a criptosistemas basados en problemas (preferiblemente <a href="https://es.wikipedia.org/wiki/NP_(clase_de_complejidad)" target="_blank">NP</a>) para los que no se conocen algoritmos cuánticos en tiempo polinomial.<br><br>
                  Los más prometedores son los basados en retículos, los basados en resolución de ecuaciones polinomiales en varias variables, y los basados en códigos correctores de errores.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/ivan_vazquez.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker8">Iván Vázquez<br>
                (<a href="https://twitter.com/IvanAgenteTutor" target="_blank">@IvanAgenteTutor</a>)
                <div class="bio8">
                  <b>Bio:</b>
                  <ul>
                    <li>Diplomado en Criminología y Criminalista</li>
                    <li>Más de 10 años de experiencia como Agente Tutor en la Policía Local de Tres Cantos (Madrid)</li>
                    <li>Voluntario en programa <a href="https://www.is4k.es/programas/cibercooperantes" target="_blank">Cibercooperantes de INCIBE</a></li>
                    <li>Miembro de <a href="https://www.x1redmassegura.com/" target="_blank">X1RedMasSegura</a></li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act8n">Mi hij@ vs su móvil
                <div class="act8d">
                  <b>Descripción:</b><br><br>
                  Información y concienciación de los riesgos a los que se enfrentan los menores al hacer un uso del móvil erróneo.<br><br>
                  <ul>
                    <li>¿Porqué enganchan tanto las redes sociales a los adolescentes?</li>
                    <li>Violación de la intimidad del menor vs patria potestad</li>
                    <li>Contrato familiar para el uso de dispositivo móvil</li>
                    <li>¿Cómo actúo si mi hij@ es víctima o agresor?</li>
                  </ul>
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/jordi_plaza.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker10">Jordi Plaza<br>
                (<a href="https://twitter.com/JordiPlazaV" target="_blank">@JordiPlazaV</a>)
                <div class="bio10">
                  <b>Bio:</b>
                  <ul>
                    <li>Graduado en Derecho y en Ciencias Políticas por la Universidad de Granada</li>
                    <li>Estudiante del Máster de Acceso al Ejercicio de la Abogacía en la Universitat Oberta de Catalunya</li>
                    <li>Estudiante del Máster en Derecho de las Telecomunicaciones, Protección de Datos, Sociedad de la Información y Audivisual en la Universidad Carlos III de Madrid</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Todo lo relativo al ejercicio de las diferentes formas de poder a través de las nuevas tecnologías</li>
                    <li>El control de los estados y empresas y la intromisión en la privacidad de los ciudadanos</li>
                    <li>El derecho fundamental a la intimidad y al secreto de las comunicaciones</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act10n">RGPqué?? y el Spanish Analytica
                <div class="act10d">
                  <b>Descripción:</b><br><br>
                  Un recorrido general sobre las novedades normativas de la UE en materia de protección de datos y un análisis crítico sobre su adaptación en el derecho español.<br><br>
                  En concreto, trataremos la actualidad sobre la polémica reforma legal
                  relativa al tratamiento de los datos políticos de los ciudadanos por parte de los partidos durante la campaña electoral.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/jorge_cuadrado.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker11">Jorge Cuadrado<br>
                (<a href="https://twitter.com/Coke727" target="_blank">@Coke727</a>)
              <div class="bio11">
                <b>Bio:</b>
                <ul>
                  <li>Grado en ingeniería informática en la Universidad de Valladolid</li>
                  <li>Máster en Ciberseguridad en la Universidad Carlos III de Madrid</li>
                  <li>Actualmente, trabajo en <a href="https://www.bbvanexttechnologies.com/" target="_blank">BBVA Next Technologies</a>, en el departamente de innovación en ciberseguridad</li>
                  <li>En los últimos años, mi foco ha sido la seguridad en cloud, dispositivos hardware, pentesting y HCI</li>
                  <li>He tratado con un gran número de tecnologías y herramientas y he creado múltiples soluciones de seguridad usando tecnologías en el estado del arte</li>
                </ul>
                <b>Intereses:</b>
                <ul>
                  <li>Me encantan los proyectos que involucran hardware, siempre me han parecido un reto divertido de resolver</li>
                  <li>Por otro lado cualquier tema que involucre privacidad y su protección me interesa, en especial me gustan los canales encubiertos y la ocultación de información</li>
                </ul>
              </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act11n">Espías domésticos: Una charla sobre asistentes de voz virtuales
                <div class="act11d">
                  <b>Descripción:</b><br><br>
                  Los asistentes de voz se han puesto de moda y han entrado en nuestros hogares. Estos dispositivos tienen un gran número de posibilidades y permiten domotizar nuestro hogar de forma sencilla.<br><br>
                  Sin embargo, surgen dudas respecto a tener micrófonos en escucha activa constante a nuestro alrededor:<br><br>
                  <ul>
                  <li>¿Qué riesgos de seguridad suponen los asistentes de voz en nuestra red interna?</li>
                  <li>¿Respetan nuestra privacidad?</li>
                  <li>¿Cuáles son sus medidas de seguridad?</li>
                  <li>¿Cómo puedo usarlos de forma segura?</li>
                  </ul>
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/jorge_romera.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker12">Jorge Romera
                <!--
              <div class="bio12">
                <b>Bio:</b>
                <ul>
                  <li></li>
                </ul>
                <b>Intereses:</b>
                <ul>
                  <li></li>
                </ul>
              </div>
              -->
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act12n">Minería en redes: Minero en el medio
                <div class="act12d">
                  <b>Descripción:</b><br><br>
                  Comenzaremos hablando sobre el famoso ataque en redes <a href="https://es.wikipedia.org/wiki/Ataque_de_intermediario" target="_blank">Man In The Middle (MITM)</a>, explicando qué podemos hacer mediante este ataque, en concreto, con la inyección de código en los paquetes de vuelta.<br><br>
                  A continuación desarrollaremos brevemente en qué se basa la <a href="https://es.wikipedia.org/wiki/Criptomoneda#Minado" target="_blank">minería de criptomonedas</a>, usando como ejemplo métodos ilegales que se está llevando a cabo recientemente en algunas páginas web, las cuáles ejecutan código JavaScript en los navegadores para que minen para los propietarios de estas webs.<br><br>
                  Finalmente, tras haber explicado cómo funciona un ataque MITM y haber hablado sobre la minería a través de la ejecución de Javascript, juntaremos ambos para hablar sobre una práctica ilegal que se ha empezado a ver en los últimos meses en algunos lugares públicos con conexión wifi gratis, donde un atacante consigue que todos los dispositivos de una red minen para él mediante inyección de código en los paquetes de vuelta a través
                  de un MITM.<br><br>
                  Todo este proceso se explicará a través de ejemplos con código un script escrito en Python conocido como <a href="https://github.com/arnaucube/coffeeMiner" target="_blank">CoffeeMiner</a>, el cual automatiza todo este proceso.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/jose_luis_navarro.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker13">José Luis Navarro<br>
                (<a href="https://twitter.com/JLNavarroAdam" target="_blank">@JLNavarroAdam</a>)
                <div class="bio13">
                  <b>Bio:</b>
                  <ul>
                    <li>"Constructor de Murallas"</li>
                    <li>Apasionado de la seguridad informática y la privacidad en Internet</li>
                    <li>Creador del firewall <a href="https://sistemasit.girsanet.com/cdc-data/" target="_blank">CDC-Data</a></li>
                    <li>Técnico Superior de Seguridad Informática y Perito Informático Judicial</li>
                    <li>Director de Proyectos e I+D en <a href="https://girsanet.com/" target="_blank">GirsaNET</a></li>
                    <li>Consultor informático en <a href="https://zinetik.com/" target="_blank">Zinetik Consultores</a></li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>La ciberseguridad en las trincheras, en primera linea, ahí donde el RedTeam y el BlueTeam se funden en uno sólo</li>
                    <li>Apasionado de Raspberry Pi como heramienta de seguridad y gestión preventiva de alertas</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act13n">Bienvenido a la república independiente de mi casa
                <div class="act13d">
                  <b>Descripción:</b><br><br>
                  Construir desde cero un sistema <a href="https://es.wikipedia.org/wiki/Sistema_de_detecci%C3%B3n_de_intrusos" target="_blank">IDS</a>, aprovechando una <a href="https://es.wikipedia.org/wiki/Raspberry_Pi#Raspberry_Pi_3_modelo_B+" target="_blank">Raspberry Pi 3 Model B+</a> con <a href="https://es.wikipedia.org/wiki/Raspbian" target="_blank">Raspbian</a> para ámbito doméstico y al alcance de cualquier persona interesada en su seguridad y privacidad empezando por la propia casa. Y quien dice casa, dice despacho profesional, oficina, lugar de trabajo...<br><br>
                  Aprovechando el doble interfaz Ethernet y wifi de Raspberry, montaremos un punto de acceso wifi seguro en el hogar, aislando nuestro propio router, para protegernos de intrusos no deseados. <br><br>
                  Instalaremos <a href="https://github.com/stamparm/maltrail" target="_blank">maltrail</a> para avisarnos de posibles virus y malware en nuestra red doméstica, y <a href="https://github.com/ntop/ntopng" target="_blank">ntopng</a> para ver y detectar problemas. Además, configuraremos los DNS de acceso a Internet para utilizar los de <a href="https://es.wikipedia.org/wiki/OpenDNS" target="_blank">OpenDNS</a> y poder crear grupos de reglas para filtrado por categorías pensando en los peques de la casa. (En caso de disponer del tiempo necesario, también usaremos <a href="https://github.com/squid-cache/squid" target="_blank">SQUID</a> + <a href="https://github.com/e2guardian/e2guardian" target="_blank">DansGuardian</a>)<br><br>
                  Todos los asistentes que aprovechen para traer su propia RPi, terminarán con la misma lista para funcionar al final del mismo. Tanto la imagen como las herramientas serán subidas a GitHub para que pueda estar al alcance de todo el mundo.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/josep_moreno.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker14">Josep Moreno (JoMoZa)<br>
                (<a href="https://twitter.com/J0MoZ4" target="_blank">@J0MoZ4</a>)
                <div class="bio14">
                  <b>Bio:</b>
                  <ul>
                    <li>Administrador de sistemas</li>
                    <li>Estudiante de Seguridad Informatica</li>
                    <li>Organizador de las Jornadas <a href="http://bitupalicante.com/" target="_blank">BitUp Alicante</a></li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Seguridad Informatica</li>
                    <li>Seguridad Web</li>
                    <li>Hacktivismo</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act14n">Tengo un perro que se llama WAF
                <div class="act14d">
                  <b>Descripción:</b><br><br>
                  Mostraremos algunas técnicas para saltar las medidas de seguridad que algunos <a href="https://es.wikipedia.org/wiki/Web_application_firewall" target="_blank">WAFs (Web Application Firewalls)</a> cuando estamos trabajando con una webshell que puede ser detectada y eliminada por estas medidas de seguridad. <br><br>
                  Con esto, mostraremos MetamorhpWebShell, un C&C que agrupa las tecnicas que se mencionan en la misma charla.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/jj_merelo.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker9">Juan Julián Merelo<br>
                (<a href="https://twitter.com/jjmerelo" target="_blank" target="_blank">@jjmerelo</a>)
                <div class="bio9">
                  <b>Bio:</b>
                  <ul>
                    <li>Profesor de Informática del <a href="https://atc.ugr.es/" target="_blank">Departamento de Arquitectura y Tecnología de Computadores de la UGR</a></li>
                    <li>Programador desde 1983</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Problemas de optimización en seguridad</li>
                    <li>Uso de Perl en cualquier cosa</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act9n">Unicode seguro con Perl 6
                <div class="act9d">
                  <b>Descripción:</b><br><br>
                  Unicode contiene gran cantidad de caracteres, algunos de los cuales no son alfanuméricos, pero se asemejan a otros caracteres alfanuméricos, lo que se puede usar para todo tipo de tropelías en formularios y demás. Por esa razón, el consorcio <a href="https://es.wikipedia.org/wiki/Unicode" target="_blank">Unicode</a> promulgó la <a href="https://www.unicode.org/reports/tr39/tr39-19.html" target="_blank">TR 39</a>, de mecanismos de seguridad en Unicode.<br><br>
                  Por otro lado, Perl 6 es uno de los lenguajes más completos en su soporte de Unicode. En esta ponencia contaremos como, en el camino de portar el módulo <a href="https://metacpan.org/pod/Unicode::Security" target="_blank">Unicode::Security</a> de Perl 5 a Perl 6, se ponen de manifiesto las diferencias entre los dos, y en qué mejora este último el soporte a este tipo de cosas.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/juan_vazquez.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker23">Juan Vázquez
                <div class="bio23">
                  <b>Bio:</b>
                  <ul>
                    <li>Grado en Ingeniería de Telecomunicaciones por la UGR</li>
                    <li>Máster Oficial de Ingeniería de Telecomunicaciones por la UGR</li>
                    <li>Máster Propio de Ciberseguridad por la UGR</li>
                    <li>Actualmente parte de <a href="https://www.jtsec.es/es/" target="_blank">jtsec Beyond IT Security</a> como consultor de Ciberseguridad</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Cualquier campo referente a la seguridad informática</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act23n">Guardianes de Internet: Funcionamiento de DNSSEC
                <div class="act23d">
                  <b>Descripción:</b><br><br>
                  El sistema de nombres de dominio (<a href="https://es.wikipedia.org/wiki/Sistema_de_nombres_de_dominio" target="_blank">Domain Name System, DNS</a>) es un sistema de nomenclatura descentralizado para dispositivos conectados a Internet o una red privada. Su función principal es traducir las direcciones IP a nombres fáciles de identificar y recordar para las personas.<br><br>
                  Este sistema asegura la disponibilidad de los sitios webs o los correos electrónicos, entre otros sistemas, mediante el mapeo de nombres de dominio con direcciones IP. Por lo tanto, el sistema DNS es una parte muy importante de Internet y su disponibilidad es crucial para el funcionamiento de Internet tal y como se conoce actualmente. Sin embargo, el diseño de este protocolo no tuvo en cuenta la seguridad.<br><br>
                  Debido a esto, se hace necesaria la utilización de un protocolo seguro como es <a href="https://es.wikipedia.org/wiki/Domain_Name_System_Security_Extensions" target="_blank">DNSSEC (Domain Name System Security Extensions)</a>. Hoy, la seguridad de Internet depende de siete llaves físicas custodiadas por 14 personas que se reúnen periódicamente. Están vigiladas por medidas de seguridad para que nadie ponga en peligro el sistema:<br><br>
                  <ul>
                    <li>¿Quiénes son estas 14 personas?</li>
                    <li>¿Son siempre las mismas?</li>
                    <li>¿Qué ocurriría si alguien consiguiera estas llaves?</li>
                  </ul><br>
                  En esta charla te contaremos todo lo que siempre quisiste saber y nunca te atreviste a preguntar sobre DNSSEC, tratando los aspectos más importantes relativos a su funcionamiento técnico y arquitectura, incluyendo información sobre el grado de implementación en la actualidad de este protocolo a nivel mundial.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/kneda.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker15">Kneda<br>
                (<a href="https://twitter.com/JR_kneda" target="_blank">@JR_kneda</a>)
                <div class="bio15">
                  <b>Bio:</b>
                  <ul>
                    <li>Postgrado en Informática Forense y Delitos Informáticos + Master en Seguridad Informática y Hacking</li>
                    <li>Trabaja como Black Box Pentester</li>
                    <li>Formador en varias materias de ciberseguridad</li>
                    <li>Perteneciente a <a href="https://hackmadrid.org/" target="_blank">HackMadrid%27</a></li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Hacking</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act15n">Cloud, tú sube que yo te robo
                <div class="act15d">
                  <b>Descripción:</b><br><br>
                  El contratar los <a href="https://es.wikipedia.org/wiki/Computaci%C3%B3n_en_la_nube" target="_blank">servicios cloud</a> para el almacenamiento de nuestros archivos permite disponer de estos en cualquier parte del mundo, pero ¿qué pasa si se vulnera la seguridad del cloud?<br><br>
                  Kneda demostrara cómo se puede llegar a vulnerar la seguridad del cloud, consiguiendo información personal; mientras, Lórien intentará a través de la forénsica, desenmascarar al autor.<br><br>
                  Se verán las técnicas utilizadas por un <a href="https://en.wikipedia.org/wiki/Red_team" target="_blank">Red Team</a> vs <a href="https://en.wikipedia.org/wiki/Blue_team_(computer_security)" target="_blank">Blue Team</a>.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/lorien.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker16">Lórien<br>
                (<a href="https://twitter.com/loriendr" target="_blank">@loriendr</a>)
                <div class="bio16">
                  <b>Bio:</b>
                  <ul>
                    <li>Ingeniero Informático</li>
                    <li>Trabaja de analista forense y DFIR</li>
                    <li>Imparte cursos de análisis forense.</li>
                    <li>Perteneciente a <a href="https://hackmadrid.org/" target="_blank">HackMadrid%27</a></li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Forense</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act16n">Cloud, tú sube que yo te robo
                <div class="act16d">
                  <b>Descripción:</b><br><br>
                  El contratar los <a href="https://es.wikipedia.org/wiki/Computaci%C3%B3n_en_la_nube" target="_blank">servicios cloud</a> para el almacenamiento de nuestros archivos permite disponer de estos en cualquier parte del mundo, pero ¿qué pasa si se vulnera la seguridad del cloud?<br><br>
                  Kneda demostrara cómo se puede llegar a vulnerar la seguridad del cloud, consiguiendo información personal; mientras, Lórien intentará a través de la forénsica, desenmascarar al autor.<br><br>
                  Se verán las técnicas utilizadas por un <a href="https://en.wikipedia.org/wiki/Red_team" target="_blank">Red Team</a> vs <a href="https://en.wikipedia.org/wiki/Blue_team_(computer_security)" target="_blank">Blue Team</a>.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/miguel_lopez.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker17">Miguel López<br>
                (<a href="https://twitter.com/WizMik12" target="_blank">@WizMik12</a>)
                <div class="bio17">
                  <b>Bio:</b>
                  <ul>
                    <li>Graduado en Matemáticas (UGR)</li>
                    <li>Master en Ciencia de Datos (UGR)</li>
                    <li>Actualmente estudiante de doctorado</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Inteligencia Artificial</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act17n">Inteligencia artificial: tu cara le suena
                <div class="act17d">
                  <b>Descripción:</b><br><br>
                  El <a href="https://es.wikipedia.org/wiki/Sistema_de_reconocimiento_facial" target="_blank">reconocimiento facial</a> es un tema muy actual; en concreto, la mayoría de smartphones y empresas multinacionales cuentan con software capaz de detectar e identificar caras en imágenes. <br><br>
                  En esta charla daremos una intuición para entender cómo funcionan por debajo estos algoritmos de inteligencia artificial. Además, comentaremos cuáles pueden ser las implicaciones sociales de este tipo de algoritmos respecto a ética o privacidad.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/patricia_saldaña.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker18">Patricia Saldaña<br>
                (<a href="https://twitter.com/pstcrimi" target="_blank">@pstcrimi</a>)
                <div class="bio18">
                  <b>Bio:</b>
                  <ul>
                    <li>Graduada en Criminología</li>
                    <li>Actualmente realizando el doctorado en Criminología</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Interesada en el estudio de la cibercriminalidad y su detección y prevención utilizando técnicas de predicción e IA</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act18n">¿Por qué los bitcoins son de interés para las organizaciones criminales?
                <div class="act18d">
                  <b>Descripción:</b><br><br>
                  De la misma forma que el resto de la sociedad, el crimen organizado se ha beneficiado de los avances tecnológicos que han surgido en la última década. Muchas de las herramientas que surgieron a partir de motivaciones lícitas están siendo utilizadas para facilitar la delincuencia escapando de la detección por parte de los cuerpos policiales.<br><br>
                  Este es el caso de la moneda virtual <a href="https://es.wikipedia.org/wiki/Bitcoin" target="_blank">bitcoin</a>, que surgió con el propósito de permitir las transacciones anónimas y descentralizadas directamente entre personas, pero más tarde, estas mismas características han atraído a organizaciones criminales que las utilizan para facilitar delitos como el blanqueo de capitales o la financiación de otras
                  actividades delictivas.<br><br>
                  De esta forma, se van a presentar las características de esta moneda virtual que resultan atractivas para este tipo de criminalidad y cómo ha tenido un papel importante en algunos casos de delincuencia organizada conocidos.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/honorablefundadora.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker19">Paula de la Hoz<br>
                (<a href="https://twitter.com/Terceranexus6" target="_blank">@Terceranexus6</a>)
                <div class="bio19">
                  <b>Bio:</b>
                  <ul>
                    <li>Analista de ciberseguridad y hacker ética</li>
                    <li>Cofundadora de <a href="https://interferencias.tech/" target="_blank">Interferencias</a></li>
                    <li>Estudio sobre derechos digitales, privacidad y responsabilidad digital</li>
                    <li>Escribo sobre seguridad en <a href="https://dev.to/terceranexus6/" target="_blank">https://dev.to/terceranexus6/</a></li>
                    <li>Hablo de tecnología en <a href="http://www.radiovallekas.org/" target="_blank">Radio Vallekas</a></li>
                    <li>Me gusta la robótica educativa y la divulgación digital</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Responsabilidad digital</li>
                    <li>Pentesting y consenso distribuído</li>
                    <li>Privacidad y censura de internet</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act19n">Introducción a hardening de Linux
                <div class="act19d">
                  <b>Descripción:</b><br><br>
                    ¡Mejor prevenir que curar! Investiguemos un poco sobre técnicas de securizacion de servidores Linux, qué revisar en nuestro sistema y qué tipo de ataques comunes podemos encontrarnos al defender un servidor Linux.<br><br>
                    Revisaremos <a href="https://es.wikipedia.org/wiki/Cron_(Unix)" target="_blank">cron jobs</a>, <a href="https://es.wikipedia.org/wiki/SELinux" target="_blank">SELinux</a>, las políticas de contraseñas, protocolos, <a href="https://es.wikipedia.org/wiki/Secure_Shell" target="_blank">SSH</a> y algunas cosas más. Casco, armadura y a la guerra.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/red.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker20">ReD
                <div class="bio20">
                  <b>Bio:</b>
                  <ul>
                    <li>Soy curioso, trasteador nato, evangelizador de la seguridad, privacidad y las tecnologías en general</li>
                    <li>Siempre me he movido por ambientes que apoyaran las libertades digitales y el conocimiento libre</li>
                    <li>Lo que me ha llevado a ser cofundador del hacklab <strong>Hackcthulhu</strong> y del hackerspace <a href="http://www.m4h.es/" target="_blank">Mad4Hacking</a></li>
                    <li>Reclamo que, aunque no se tenga nada que esconder, el anonimato sea un derecho</li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Sistemas de anonimato</li>
                    <li>Criptografía</li>
                    <li>Análisis forense</li>
                    <li>Pentesting</li>
                    <li>En general todo lo relacionado con la seguridad</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act20n">Sistemas biométricos, quién dijo miedo
                <div class="act20d">
                  <b>Descripción:</b><br><br>
                  Poco a poco los <a href="https://es.wikipedia.org/wiki/Biometr%C3%ADa" target="_blank">sistemas biométricos</a> se han ido introduciendo en nuestras vidas, llegando a ser habitual interactuar con ellos de forma diaria. Algunos pasan desapercibidos a nuestra atención, realizando su trabajo de forma silenciosa. Otros somos conscientes que están, pero desconocemos los detalles de como funcionan.<br><br>
                  En esta charla, recorreremos los sistemas biométricos más comunes, en qué se basa su funcionamiento y cómo se pueden "llevar al limite" para que actúen de forma no esperada ;-).
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/rolo_mijan.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker21">Rolo Miján<br>
                (<a href="https://twitter.com/RoloMijan" target="_blank">@RoloMijan</a>)
                <div class="bio21">
                  <b>Bio:</b>
                  <ul>
                    <li>Estudiante de Ingeniería informática en la UNED</li>
                    <li>Analista de seguridad en <a href="https://wsg127.com/" target="_blank">WISE Security Global</a></li>
                    <li>ExStaff del foro y blog <a href="https://underc0de.org/" target="_blank">Underc0de.org</a></li>
                    <li>Escritor en <a href="https://www.hackingdesdecero.org/" target="_blank">hackingdesdecero.org</a></li>
                    <li>Escritor en <a href="https://www.whateversec.com/" target="_blank">whateversec.com</a></li>
                  </ul>
                  <b>Intereses:</b>
                  <ul>
                    <li>Conocimientos en seguridad informática enfocados al pentesting web pentesting</li>
                    <li>Conocimientos básicos en programación y análisis de malware</li>
                    <li>Programación en C y PHP</li>
                    <li>Interés en sistemas Linux</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act21n">Jugando con flujos como un buen hacker
                <div class="act21d">
                  <b>Descripción:</b><br><br>
                  En ocasiones las aplicaciones webs, tanto de tiendas online, como bancos, como cualquier otra, hacen uso de flujos de acciones para poder dar un servicio a sus clientes. La creación de estos flujos trae consigo fallas de seguridad
                  que una persona podría explotar.<br><br>
                  En esta charla aprenderemos a jugar con estos flujos, para poder aprovecharnos de su funcionamiento.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/sandra_haro.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker22">Sandra Haro<br>
                (<a href="https://twitter.com/sanharom" target="_blank">@sanharom</a>)
                <div class="bio22">
                  <b>Bio:</b>
                  <ul>
                    <li>Politóloga con Máster en Cultura de Paz, Conflictos y DDHH</li>
                    <li>Actualmente personal investigador de <a href="https://medialab.ugr.es" target="_blank">Medialab UGR - Laboratorio de Investigación en Cultura y Sociedad Digital</a></li>
                  </ul>
                  <b>Interés:</b>
                  <ul>
                    <li>Interés en Humanidades Digitales</li>
                  </ul>
                </div>
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act22n">Voto electrónico: actualidad, potencialidades y retos de futuro
                <div class="act22d">
                  <b>Descripción:</b><br><br>
                  Análisis politológico de este sistema de votación que tantas controversias produce. Actualidad, ensayos y países que ya han implementado su uso. Resultados, potencialidades y retos de futuro.<br><br>Una reflexión desde la seguridad y la ética ciudadana.
                </div>
              </div>
            </td>
          </tr>

          <!-- ############ PONENTE ############ -->

          <tr>
            <td style="vertical-align: middle;">
              <img src="{{ site.url }}{{ "/assets/images/jasyp/19/participantes/darkmesh.jpg" }}" height="250" width="250">
            </td>
            <td style="vertical-align: middle;">
              <div class="speaker24">The DarkMesh Staff
              </div>
            </td>
            <td style="vertical-align: middle;">
              <div class="act24n">Protección de activistas pro DDHH en la red, colaboración entre comunidades y herramientas de seguridad DIY
                <div class="act24d">
                  <b>Descripción:</b><br><br>
                  DarkMesh es una red ideada para hackers con la idea de experimentar e intercambiar conocimientos de proyectos. La comunidad a lo largo del mundo de los hackers es algo singular e incierto al mismo tiempo, y por ello, la iniciativa está servida. En la era de los 80 y 90 existían grupos fuertes con gran afán por investigar y compartir. Es el momento de que grupos que ahora mismo están disgregados puedan conocerse y compartir.<br><br>
                  Requisitos del asistente: PC o Mac con Linux/Windows/macOS, <a href="https://es.wikipedia.org/wiki/Raspberry_Pi" target="_blank">Raspberry</a> Pi 3 mínimo o con módulo de Wifi instalado e imagen de Raspbian descargada e instalada (también se puede hacer con un ordenador portátil). A ser posible, tener <a href="https://es.wikipedia.org/wiki/Docker_(software)" target="_blank">Docker</a> instalado.<br><br>
                  Enlaces de la descarga de Raspbian: <a href="https://www.raspberrypi.org/downloads/" target="_blank">https://www.raspberrypi.org/downloads/</a>
                </div>
              </div>
            </td>
          </tr>

        </tbody>
      </table>
    </div>

  </div>

  <div class="bootstrap">
    <div class="text-center">
      <br>
      <h3 id="horario">Horario</h3>
      <hr>
      <div>
      <table class="tg">
      <tr>
        <th class="tg-amwm" colspan="3">VIERNES 26</th>
      </tr>
        <tr>
          <td class="tg-41e9">HORA</td>
          <td class="tg-41e9">PONENTE</td>
          <td class="tg-41e9">TÍTULO</td>
        </tr>
        <tr>
          <td class="tg-baqh">10:00-10:20</td>
          <td class="tg-baqh" colspan="2">APERTURA</td>
        </tr>
        <tr>
          <td class="tg-0l6a">10:20-10:45</td>
          <td class="tg-0l6a" colspan="2">PRESENTACIÓN</td>
        </tr>
        <tr>
          <td class="tg-baqh">10:50-11:10</td>
          <td class="tg-baqh">Alex García</td>
          <td class="tg-baqh">Juego de Trazas</td>
        </tr>
        <tr>
          <td class="tg-0l6a">11:15-11:35</td>
          <td class="tg-0l6a">Patricia Saldaña</td>
          <td class="tg-0l6a">¿Por qué los bitcoins son de interés para las organizaciones criminales?</td>
        </tr>
        <tr>
          <td class="tg-baqh">11:40-12:10</td>
          <td class="tg-baqh" colspan="2">DESCANSO</td>
        </tr>
        <tr>
          <td class="tg-0l6a">12:10-12:50</td>
          <td class="tg-0l6a">Bart Ortiz</td>
          <td class="tg-0l6a">Hacking AI: Inteligencia Artificial y Seguridad</td>
        </tr>
        <tr>
          <td class="tg-baqh">12:55-13:35</td>
          <td class="tg-baqh">Fco. Javier Lobillo</td>
          <td class="tg-baqh">Criptografía post-cuántica</td>
        </tr>
        <tr>
          <td class="tg-0l6a">13:40-14:00</td>
          <td class="tg-0l6a">Antonio Gámiz</td>
          <td class="tg-0l6a">Open Authentication (OAuth) 2.0</td>
        </tr>
        <tr>
          <td class="tg-baqh">14:05-15:30</td>
          <td class="tg-baqh" colspan="2">COMIDA</td>
        </tr>
        <tr>
          <td class="tg-0l6a">15:30-15:50</td>
          <td class="tg-0l6a">Rolo Miján</td>
          <td class="tg-0l6a">Jugando con flujos como un buen hacker</td>
        </tr>
        <tr>
          <td class="tg-baqh">15:55-16:35</td>
          <td class="tg-baqh">Iván Vázquez</td>
          <td class="tg-baqh">Mi hij@ vs su móvil</td>
        </tr>
        <tr>
          <td class="tg-0l6a">16:40-17:00</td>
          <td class="tg-0l6a">Sandra Haro</td>
          <td class="tg-0l6a">Voto electrónico: actualidad, potencialidades y retos de futuro </td>
        </tr>
        <tr>
          <td class="tg-baqh">17:05-17:45</td>
          <td class="tg-baqh">Ale Cortés</td>
          <td class="tg-baqh">Tecnoadicción, la droga del siglo XXI</td>
        </tr>
        <tr>
          <td class="tg-0l6a">17:50-18:10</td>
          <td class="tg-0l6a" colspan="2">DESCANSO</td>
        </tr>
        <tr>
          <td class="tg-baqh">18:10-18:50</td>
          <td class="tg-baqh">Jorge Cuadrado</td>
          <td class="tg-baqh">Espías domésticos: Una charla sobre asistentes de voz virtuales</td>
        </tr>
        <tr>
          <td class="tg-0l6a">18:55-19:15</td>
          <td class="tg-0l6a">Celia Pedregosa (AIKUME)</td>
          <td class="tg-0l6a">Animales en la informática</td>
        </tr>
        <tr>
          <td class="tg-baqh">19:20-20:00</td>
          <td class="tg-baqh">ReD</td>
          <td class="tg-baqh">Sistemas biométricos, quién dijo miedo</td>
        </tr>
      </table>
      </div>

      <br><br>

      <div>
      <table class="tg">
        <tr>
          <th class="tg-amwm" colspan="3">SÁBADO 27<br></th>
        </tr>
        <tr>
          <td class="tg-41e9">HORA</td>
          <td class="tg-41e9">PONENTE</td>
          <td class="tg-41e9">TÍTULO</td>
        </tr>
        <tr>
          <td class="tg-baqh">10:30-11:10</td>
          <td class="tg-baqh">Paula de la Hoz</td>
          <td class="tg-baqh">Introducción a hardening de Linux</td>
        </tr>
        <tr>
          <td class="tg-0l6a">11:15-11:35</td>
          <td class="tg-0l6a">Juan Julián Merelo</td>
          <td class="tg-0l6a">Unicode seguro con Perl 6</td>
        </tr>
        <tr>
          <td class="tg-baqh">11:40-12:10</td>
          <td class="tg-baqh" colspan="2">DESCANSO</td>
        </tr>
        <tr>
          <td class="tg-0l6a">12:10-12:50</td>
          <td class="tg-0l6a">José Luis Navarro</td>
          <td class="tg-0l6a">Bienvenido a la república independiente de mi casa</td>
        </tr>
        <tr>
          <td class="tg-baqh">12:55-13:55</td>
          <td class="tg-baqh">Kneda y Lórien</td>
          <td class="tg-baqh">Cloud, tú sube que yo te robo</td>
        </tr>
        <tr>
          <td class="tg-0l6a">14:00-15:30</td>
          <td class="tg-0l6a" colspan="2">COMIDA</td>
        </tr>
        <tr>
          <td class="tg-baqh">15:30-15:50</td>
          <td class="tg-baqh">Josep Moreno (JoMoZa)</td>
          <td class="tg-baqh">Tengo un perro que se llama WAF</td>
        </tr>
        <tr>
          <td class="tg-0l6a">15:55-16:15</td>
          <td class="tg-0l6a">Jorge Romera</td>
          <td class="tg-0l6a">Minería en redes: Minero en el medio</td>
        </tr>
        <tr>
          <td class="tg-baqh">16:20-17:20</td>
          <td class="tg-baqh">The DarkMesh Staff</td>
          <td class="tg-baqh">Protección de activistas pro DDHH en la red, colaboración entre comunidades y herramientas de seguridad DIY</td>
        </tr>
        <tr>
          <td class="tg-0l6a">17:25-17:45</td>
          <td class="tg-0l6a">Ernesto Serrano</td>
          <td class="tg-0l6a">Sorry, but your common sense is in another castle: seguridad informática for dummies</td>
        </tr>
        <tr>
          <td class="tg-baqh">17:50-18:20</td>
          <td class="tg-baqh" colspan="2">DESCANSO</td>
        </tr>
        <tr>
          <td class="tg-0l6a">18:20-19:00</td>
          <td class="tg-0l6a">Miguel López</td>
          <td class="tg-0l6a">Inteligencia artificial: tu cara le suena</td>
        </tr>
        <tr>
          <td class="tg-baqh">19:05-19:45</td>
          <td class="tg-baqh">Juan Vázquez</td>
          <td class="tg-baqh">Guardianes de Internet: Funcionamiento de DNSSEC</td>
        </tr>
        <tr>
          <td class="tg-0l6a">19:50-20:10</td>
          <td class="tg-0l6a">Jordi Plaza</td>
          <td class="tg-0l6a">RGPqué?? y el Spanish Analytica</td>
        </tr>
        <tr>
          <td class="tg-baqh">20:15-21:00</td>
          <td class="tg-baqh" colspan="2">MESA REDONDA + CIERRE</td>
        </tr>
      </table>
      </div>
    </div>
  </div>

  <div class="bootstrap">
    <div class="text-center">
      <br>
      <h3 id="hb">Hack & Beers Granada Vol. 5</h3>
      <hr>
      <img src="/assets/images/jasyp/18/beer.png" style="max-width:50%;">
      <br>
    </div>
  </div>

Esta será la <strong>5ª edición de Hack & Beers en Granada</strong>, siguiendo el mismo planteamiento de siempre: charlas sobre temáticas relacionadas con seguridad informática tomándose unas cervezas a cuenta de su patrocinador [Sophos Iberia](https://twitter.com/SophosIberia). En esta ocasión cuenta con las siguientes charlas:

<ul>
<li><strong><a href="https://twitter.com/Kirzahk" target="_blank">Ale Cortés</a>: "Quantum Hacker"</strong></li>
<li><strong><a href="https://twitter.com/_albertocasares" target="_blank">Alberto Casares</a>: "Disrupting adversaries using identity intelligences & attribution"</strong></li>
</ul>

Tendrá lugar el jueves 25 a partir de las 18:00 en <a href="https://www.openstreetmap.org/node/5907687908" target="_blank"><strong>La Posada <i>(Calle Periodista Eugenio Selles, 10)</i></strong></a> y aunque la entrada es gratuita, sí que desde la organización de Hack&Beers nos piden que toda las personas que vayan a asistir se registren en [esta página del evento](https://hackandbeers.es/events/hack-beers-granada-vol-5/) para gestionar el tema de las cervezas.

<div class="bootstrap">
  <div class="text-center">
      <img src="/assets/images/jasyp/19/actividades/hbgranada.jpg" height="50%" alt="" />
  </div>
</div>

  <div class="bootstrap">
    <div class="text-center">
      <br>
      <h3>¿Te hemos convencido?</h3>
      <hr>
    </div>
  </div>

  Pues entonces ya sabes, te esperamos **los días VIERNES 26 Y SÁBADO 27 DE ABRIL** en la **[Escuela Técnica Superior de Ingenierías Informática y de Telecomunicación de la Universidad de Granada](https://etsiit.ugr.es/)** _(Calle Periodista Daniel
  Saucedo Aranda, s/n, 18071 Granada)_. Todas las personas son bienvenidas independientemente de edades, géneros, etnias y condición social; tengas conocimientos o no sobre los temas, lo importante es que tengas interés. **Seas quien seas, si te
  gusta la temática ¡Anímate!**

  Por otra parte, la asistencia al evento es totalmente gratuito y libre, aunque si no te importa, puedes registrarte en [este evento de Meetup](https://www.meetup.com/es-ES/Granada-Geek/events/260174686/) para hacer estimación de público asistente.

  Y ya para terminar, lo único que querríamos pediros es que nos ayudarais con la publicidad del evento por redes sociales y demás canales de difusión que tengáis a vuestra disposición, ya sea usando el hashtag #jasyp2019 ([como estamos haciendo en
  Twitter](https://twitter.com/search?f=tweets&q=%23jasyp2019)) o mencionándonos directamente por Twitter como [@Inter_ferencias](https://twitter.com/Inter_ferencias) o por Mastodon como
  [@interferencias@mastodon.technology](https://mastodon.technology/@interferencias).

  ![mosaico_v_2018]({{ "/assets/images/jasyp/18/charlas/mosaico_v.jpg" }})

  <div class="bootstrap">
    <div class="text-center">
      <br>
      <h3>Agradecimientos</h3>
      <hr>
    </div>
  </div>

  <figure class="half">
    <a href="https://www.ugr.es/" target="_blank">
      <img src="/assets/images/jasyp/19/agradecimientos/ugr.png" alt="" />
    </a>
    <a href="https://etsiit.ugr.es/" target="_blank">
      <img src="/assets/images/jasyp/19/agradecimientos/etsiit.png" alt="" />
    </a>
  </figure>

  <figure class="half">
    <a href="https://www.hackingdesdecero.org/" target="_blank">
      <img src="/assets/images/jasyp/19/agradecimientos/hdc.png" alt="" />
    </a>
    <a href="https://twitter.com/hackandbeers" target="_blank">
      <img src="/assets/images/jasyp/19/agradecimientos/hb.png" alt="" />
    </a>
  </figure>

  <figure class="third" style="display: flex; align-items: center; justify-content: center">
    <a href="https://hackmadrid.org/" target="_blank">
      <img src="/assets/images/jasyp/19/agradecimientos/hackmadrid.png" alt="" />
    </a>
    <a href="http://www.bitupalicante.com/" target="_blank">
      <img src="/assets/images/jasyp/19/agradecimientos/bitup.png" alt="" />
    </a>
    <a href="https://www.fwhibbit.es/" target="_blank">
      <img src="/assets/images/jasyp/19/agradecimientos/fwhibbit.png" alt="" />
    </a>
  </figure>
