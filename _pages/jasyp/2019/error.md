---
layout: page
title: JASYP '19 - Participación
permalink: /jasyp/19/error/
description: "Formulario de participación para las JASYP '19"
image:
  feature: banners/header.jpg
timing: false
share: false
---

Se ha producido un error al intentar registrar tu solicitud de participación en las **JASyP '19**.

Por favor, vuelve a intentarlo pasado unos minutos y en caso que el problema siga produciéndose escríbenos a [jasyp@interferencias.tech](mailto:jasyp@interferencias.tech).

Saludos,
![cartel_privacidad_etsiit]({{ "/assets/images/social/tags/banner.png" }})
