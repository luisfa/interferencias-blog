---
layout: page
title: JASYP '19 - Girls 4 Privacy
permalink: /jasyp/19/g4p/
description: "Girls 4 Privacy"
image:
  feature: banners/header.jpg
timing: false
share: false
---

![banner]({{ "/assets/images/jasyp/19/actividades/g4p.png" }})

Desde su inicio, Interferencias **ha estado muy ligado a apoyar el desarrollo de las mujeres en tecnología**, especialmente en ciberseguridad y privacidad. En anteriores ediciones, el grupo se ha volcado en facilitar la representación femenina en el sector, impulsando la voz de varias mujeres.

En esta edición queremos apoyar el talento femenino, y por ello lanzamos **"Girls 4 Privacy"**. Queremos entregarle **una beca a una o varias mujeres que presenten un proyecto relacionado con la temática del evento** (seguridad, privacidad y derechos digitales) con la intención de impulsar dicho proyecto.

La beca se entregará por proyecto, ya sea individual o en grupo, la única regla es que éste sea llevado por mujeres.

Dicha selección será llevada acabo por un jurado que conforman los organizadores del evento y un representante de cada uno de los patrocinadores de nivel [**"Digital Hero"**]({{ site.url }}/jasyp/patrocinio).
