---
layout: page
title: JASYP '19 - Concurso de arte
permalink: /jasyp/19/arte/
description: "Concurso de arte para las JASYP"
image:
  feature: banners/header.jpg
timing: false
share: false
---

![banner]({{ "/assets/images/jasyp/19/actividades/arte.png" }})

Con ánimo de impulsar la **multidisciplinaridad**, en la edición anterior sacamos un concurso de arte de la temática del evento. La idea de dicho concurso era **impulsar la capacidad de transmitir conceptos**, encontrar un puente entre la teoría técnica y los usuarios.

Con este objetivo en mente, en las [JASYP '18']({{ site.url }}/jasyp) del año pasado lanzamos por primera vez un concurso de arte que tenemos intención de repetir este año. El tema de las obras debía ser **Privacidad en Internet, Derechos digitales, Seguridad informática o Anonimato en las redes**; utilizando como medio de expresión pintura, fotografía, ilustración, escultura, modelado 3D, recurso audiovisual, o en general, tipo de técnica o formato de representación, no siendo esto un punto relevante para la decisión final.

Este año volveremos a hacer una nueva edición de este concurso, donde un jurado conformado por organizadores del evento y consultores externos del área artística, así como un representante de cada uno de los patrocinadores de nivel [**"Super User"**]({{ site.url }}/jasyp/patrocinio/).

{% capture images %}
  /assets/images/jasyp/18/arte/exposicion.jpeg
{% endcapture %}
{% include gallery images=images cols=1 %}

Las obras fueron expuestas durante las jornadas en el hall de la Escuela Técnica Superior de Ingenierías Informática y de Telecomunicación de la Universidad de Granada, **siendo ganador de un vale para la tienda de bellas artes [La Madriguera Shop](http://lamadriguerashop.com/es/) la obra de [Ángel Pablo Hinojosa](http://www.psicobyte.com/): Beyond the Nipple**, obra que el mismo autor explicada de la siguiente forma:

<blockquote>
Si miramos más allá de las políticas de "Nada de pezones" por las que son conocidas redes sociales como Facebook, Instagram u otros, podemos ver que en todo el mundo se están censurando imágenes, textos y contenidos de todo tipo por motivos políticos, religiosos, raciales, ideológicos, de género o, generalmente, sin alegar justificación alguna.

Esta obra es un mosaico creado a partir de una selección de 365 imágenes que, en algún momento, han sido censuradas en una u otra red social.
</blockquote>

{% capture images %}
  /assets/images/jasyp/18/arte/hinojosa_angel_pablo.png
  /assets/images/jasyp/18/arte/hinojosa_angel_pablo_2.png
{% endcapture %}
{% include gallery images=images cols=2 %}

Agradecemos enormemente a todos los participantes su esfuerzo y nos gustaría poder darles el mayor de los reconocimientos, por eso queremos que aquí quedén expuestas todas las obras del resto de partipantes. Así lo único que nos queda es volver a decíroslo: **¡¡¡GRACIAS!!!**

{% capture images %}
  /assets/images/jasyp/18/arte/arellano_belen.jpg
{% endcapture %}
{% include gallery images=images cols=1 %}
<h3 style="text-align: center; margin-top: -1em">Belén Arellano - Sin título</h3>

{% capture images %}
  /assets/images/jasyp/18/arte/bailen_sara.jpg
{% endcapture %}
{% include gallery images=images cols=1 %}
<h3 style="text-align: center; margin-top: -1em">Sara Bailén - Críticas destructivas anónimas</h3>

{% capture images %}
  /assets/images/jasyp/18/arte/cueto_candela.jpg
{% endcapture %}
{% include gallery images=images cols=1 %}
<h3 style="text-align: center; margin-top: -1em">Candela Cueto <a href="https://incandescencia.wixsite.com/incandescente" target="_blank">(Portfolio)</a> - Daily</h3>

{% capture images %}
  /assets/images/jasyp/18/arte/juste_iñigo.jpg
{% endcapture %}
{% include gallery images=images cols=1 %}
<h3 style="text-align: center; margin-top: -1em">Íñigo Juste - World Wide Sauron</h3>

<figure>
  <iframe src="https://player.vimeo.com/video/264333248" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  <h3 style="text-align: center;">Mística Virtual <a href="http://misticavirtual.com/" target="_blank">(Web)</a> - #{Identidades}</h3>
</figure>
