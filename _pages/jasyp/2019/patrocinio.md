---
layout: page
title: JASYP '19 - Patrocinio
permalink: /jasyp/19/patrocinio/
description: "Patrocinar las JASYP '19"
image:
  feature: banners/header.jpg
timing: false
share: false
---

![banner]({{ "/assets/images/social/tags/banner.png" }})

Las **JASYP**, al igual que todo lo que hacemos desde **Interferencias**, lo hacemos *sin ánimo de lucro alguno*, simplemente lo hacemos porque creemos en nuestros ideales y consideramos que muchas cosas que a nosotros nos resultan interesantes o necesarias conocer, también pueden ser consideradas de ese modo por otras personas. Y ahora, más que nunca, creemos que la conciencia digital es necesaria. Necesitamos aprender cada día más sobre seguridad, privacidad y derechos digitales para comprender mejor como funciona nuestro mundo.

Hay unos gastos que al ser pequeños podemos asumirlos (como puede ser el coste de nuestro servidor y el dominio web), pero hay otras gastos que nos es imposible tomar (alquiler de espacios o correr con los gastos de invitados para nuestras actividades).

Para el tema de espacios, hemos tenido la suerte que siempre hemos contado con el apoyo absoluto de la dirección actual de la [Escuela Técnica Superior de Ingenierías Informática y de Telecomunicación de la Universidad de Granada](https://etsiit.ugr.es/), aunque en la provincia de Granada eventualmente también hemos realizado eventos en otros espacios. Es por eso que también buscamos patrocinadores que nos permitieran hacer frente a los gastos que serían necesarios cubrir para facilitar la intervención en las Jornadas de todos los participantes sin importar de qué parte del país sean.

<figure>
  <img src="/assets/images/jasyp/18/agradecimientos/etsiit.jpg" alt="inauguración">
  <figcaption>D. Joaquin Fernandez-Valdivia, director de la ETSIIT, abriendo las JASYP con unas palabras de reconocimiento</figcaption>
</figure>

A todos los patrocinadores del evento se les ofrece por tanto, visibilidad en este tipo de comunidades, así como en la prensa provincial gracias a la mediación de la Dirección de Escuela Técnica donde se realizaran parte de las Jornadas. El perfil de los seguidores comprende un rango muy amplio, siempre relacionado con la tecnologia, pero desde muchas áreas. Las JASYP son el evento principal del grupo, de modo que durante los días que dura, varios grupos y comunidades que se reunen para tratar la temática principal tendrán una actividad mucha más intensa en las redes sociales, lo que conlleva una mayor visibilidad en comparación al resto del año.

Interferencias es uno de los grupos más activos de la comunidad informática en Granada, teniendo además una gran actividad y seguimiento en varias plataformas, como Twitter y Telegram, que sirven como punto de encuentro para intercambiar opiniones e información, y un blog propio en el que regularmente se escriben artículos técnicos o de opinión.

Dado que nació en un ámbito universitario, Interferencias procura seguir teniendo presencia en ese entorno, pero también participar en los eventos más conocidos relacionados con ciberseguridad tanto en España como en Europa: [RootedCON](https://www.rootedcon.com/), [Navaja Negra](https://www.navajanegra.com/), [EuskalHack](https://securitycongress.euskalhack.org/), [DeepSec](https://deepsec.net/) (Viena), [FOSDEM](https://fosdem.org/) (Bruselas) entre otros. Además, el grupo siempre está abierto a colaborar con otros grupos que estén relacionadas con el software libre y el desarrollo tecnológico abierto.

<figure>
  <img src="/assets/images/jasyp/18/agradecimientos/fundadora.jpeg" alt="inauguración">
  <figcaption>Paula de la Hoz, fundadora de Interferencias, durante su charla en el FOSDEM 2019</figcaption>
</figure>

Actualmente hemos definido tres niveles de patrocinio:

- **{H}AC{K}TIVISTA (>=200€)**:
  - Inclusión del logo en carteles y tríptico informativo del evento
  - Mención en el evento, redes sociales y en la página oficial

- **Super User (>=500€)**:
  - Inclusión del logo en carteles y tríptico informativo del evento
  - Mención en el evento, redes sociales y en la página oficial
  - Patrocinador oficial a elegir entre:
    - [Captura de la bandera]({{ site.url }}/jasyp/ctf/)
    - [Concurso de arte]({{ site.url }}/jasyp/arte/)

- **Digital Hero (>=1000€)**:
  - Representación de una persona en la mesa redonda principal del evento, en donde se tratarán temas de actualidad relacionadas con ciberseguridad y privacidad
  - Inclusión del logo en todos los trípticos informativos, regalos y carteles del evento
  - Mención de honor al principio y final del evento
  - Mención en redes sociales y en la página web oficial del evento
  - Patrocinador oficial de **Girls 4 Privacy**

También puedes encontrar toda la información sobre las oportunidades de patrocinio en [este documento]({{ site.url }}/assets/pdfs/Call4Patreon_JASYP2019.pdf).

Si quieres participar como patrocinador de las **JASYP '19** o tienes alguna duda relacionada con las Jornadas escríbemos a [jasyp@interferencias.tech](mailto:[jasyp@interferencias.tech]).
