---
layout: post
title: 10º aniversario de Tails
author: germaaan
image:
  feature: banners/header.jpg
tags: tails anonimato privacidad sistemas-operativos
---

Hace unos días fue el 10º cumpleaños de [Tails](https://tails.boum.org/) (The amnesic incognito live systema), un sistema operativo creado con el objetivo de ayudarnos a preservar nuestra privacidad y anonimato. Aunque sabemos que es bastante popular en este ámbito, nunca está de mal dedicarnos un momento a hablar sobre él.

<img src="{{ site.url }}/assets/images/tails/tails.png" style="display: block; margin: 0 auto;">

Fue el 16 de agosto de 2009 cuando se anunció la publicación de su versión inicial en la lista de correo de [Tor Project](https://www.torproject.org/) con su nombre original: "amnesia".

[Announce: amnesia Live system (initial release)](https://lists.torproject.org/pipermail/tor-talk/2009-August/002667.html)

Por si alguien no conoce la red Tor (que lo dudamos 🌚), para no extendernos mucho, es una red de comunicaciones distribuida alrededor del mundo compuesta por nodos de voluntarios, con el objetivo de anonimizar el origen de las peticiones a recursos web.

<img src="{{ site.url }}/assets/images/tails/tor.gif" style="display: block; margin: 0 auto;">

Cuando "amnesia" fue presentado, lo hizo como un sistema operativo "live" destacando dos características: todo el tráfico saliente hacia Internet era forzado a salir por la red Tor y no dejaría rastro en el almacenamiento local del dispositivo salvo que lo indique explícitamente. La idea no era nueva, y ellos mismos reconocen que estaban muy inspirados en "Incognito", otra distribución similar basada en [Gentoo](https://www.gentoo.org/), pero que fue discontinuada a los pocos meses de lanzar su primera versión.

El objetivo primordial era muy simple: poder usar Internet de forma anónima con el objetivo con el objetivo de burlar la censura, algo que cada día que vemos que es más necesario para poder saber qué está pasando realmente en zonas en conflicto del mundo.

[21st-century censorship](https://archives.cjr.org/cover_story/21st_century_censorship.php)

Como decíamos, Tails fuerza que todas las conexiones se hagan a través de la red Tor para evitar que se conozca el origen de las mismas, pero eso no es suficiente para proteger las comunicaciones, también es necesario cifrar el contenido de las mismas. Por un lado, para el cifrado de disco tenemos incluido [LUKS](https://gitlab.com/cryptsetup/cryptsetup), pero también tiene soporte para VeraCrypt(https://veracrypt.fr/en/Home.html).

La principal ventaja de #VeraCrypt es que nos permite usar contenedores ocultos y varias contraseñas de descifrado. Imaginemos que nos obligan a desbloquear nuestro sistema, pero no queremos que conozcan de la existencia de determinada información en el mismo. Mediante este mecanismo podemos conseguir que al introducir una contraseña concreta se hagan accesible solo los archivos corrientes que nos da igual que sean visibles, mientras que para acceder a la información que queremos ocultar sea necesario introducir una contraseña distinta. A simple vista no se podría conocer que existen más datos de los que realmente estamos mostrando, esto es lo que se conoce como cifrado de negación plausible; es fácilmente creíble que no haya más datos cifrados de los que mostramos.

[VeraCrypt >> Documentation >> Plausible Deniability >> Hidden Volume](https://www.veracrypt.fr/en/Hidden%20Volume.html)

<img src="{{ site.url }}/assets/images/tails/veracrypt.png" style="display: block; margin: 0 auto;">

Existe cierta controversia con VeraCrypt ya que al ser un fork de otra aplicación, TrueCrypt (discontinuada y con problemas de seguridad reconocidos), heredó también su licencia, una licencia propia que nunca fue reconocida como oficialmente abierta. Aunque todo el trabajo nuevo haya sido añadido bajo una licencia libre [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0), el código legacy que todavía contiene hace que VeraCrypt no se pueda considerar software libre (lo que impide que esté en muchos repositorios de distribuciones GNU/Linux).

Para que nuestra comunicaciones puedan ser seguras, también es vital que podamos cifrar el contenido de nuestros mensajes y firmarlos de forma que se sepa de forma inequívoca que realmente han sido escritos por nosotros. Esto lo podemos hacer gracias a [OpenPGP](https://www.openpgp.org/), pudiendo cifrar textos con una frase-contraseña o, lo realmente interesante, también cifrar mensajes de correo electrónico y firmarlos con una llave pública. Para esto último, necesitamos antes crearnos una pareja de llaves pública/privada, usando una para cifrar y otra para descifrar. Con esto podremos conseguir que alguien se pueda asegurar que el texto que nos mande, solo pueda ser leído por nosotros. Ya que conociendo nuestra clave pública (que podemos subir a sitios como [https://pgp.mit.edu](https://pgp.mit.edu) o simplemente publicar en nuestras redes), se cifra un mensaje con ella que solo se podrá descifrar con nuestra clave privada (que solo nosotros deberíamos tener).

<img src="{{ site.url }}/assets/images/tails/cifrado_clave_publica.jpg" style="display: block; margin: 0 auto;">

Además de esto, también podemos encontrar muchas otras aplicaciones útiles como generador aleatorio de contraseñas ([PWGen](https://sourceforge.net/projects/pwgen/)), un teclado virtual (por si sospechamos que hay un keylogger en el sistema), limpiador de metadatos ([MAT](https://mat.boum.org/)), gestor contraseñas ([KeePassX](https://www.keepassx.org/))...

Usar un gestor de contraseñas en combinación con un generador aleatorio es especialmente interesante porque permite aumentar enormemente la fortaleza de las mismas sin tener que preocuparnos de recordarlas extensas cadenas de texto. Pudiendo además pasar la dificultad para romperla de horas a años y de meses a milenios en función de la longitud y de si además de minúsculas se usan mayúsculas, números y otros símbolos.

También los metadatos es algo que muchas veces no se tienen en cuenta, pero con los que sin saberlo podemos estar dando información relativa a nuestra localización geográfica o con quien nos comunicamos a determinadas horas y con que frecuencia. El problema de esto es que puede desvelar patrones, relaciones y comportamientos cotidianos sobre nosotros de una forma mucho más fácil que si hubiera que analizar el contenido de nuestros mensajes (lo que es más complicado) o inviable (cuando la cantidad de mensajes es alta). Y como toda información personal, siempre habrá alguien que intentará sacar un beneficio económico del mismo, porque además, los metadatos no están regulados como información personal.

[¿Quién nos protege de nuestros metadatos?](https://www.publico.es/sociedad/proteccion-datos-metadatos.html)

Lo mejor, es que como es un sistema live (que además tiene opciones para asegurar el borrado seguro de archivos) podemos estar tranquilos de que una vez que dejemos el ordenador, no habremos dejado rastro en él, todo el sistema se ejecuta entre la unidad USB y la memoria RAM.

La utilidad de Tails es tal que no es de extrañar que haya recibido el reconocimiento y apoyo de diversos proyectos tan relevantes como el propio [Tor Project](https://www.torproject.org/), [Debian Project](https://www.debian.org/) (en cuyo sistema operativo se basa el proyecto), [Mozilla Foundation](https://foundation.mozilla.org/es/) o [Freedom of the Press Foundation](https://freedom.press/).

Aunque sin duda, cuando Tails llegó a su máximo punto de popularidad fue a raíz de que [Edward Snowden lo recomendara](https://twitter.com/Snowden/status/941018955405242369) como el sistema más accesible para personas corrientes, acercándoles a Tor y Linux con algo que pueden llevar en un pendrive en su día a día.

Todo esto se magnificó teniendo en cuenta que veníamos de una época en la que se acababa de confirmar lo que se venía sospechando desde hace tiempo, la NSA tenía varios programas de vigilancia masiva en funcionamiento desde hace tiempo.

[Todos los programas de espionaje de la NSA desvelados por Snowden](https://www.eldiario.es/turing/vigilancia_y_privacidad/NSA-programas-vigilancia-desvelados-Snowden_0_240426730.html)

Y para que todas las intenciones de Tails queden los más claras posibles, al igual que otros proyectos (como Debian) tienen un contrato social en que indican claramente su compromiso con el anonimato, la privacidad y el software libre.

[Tails - Social Contract](https://tails.boum.org/contribute/working_together/social_contract/)

Hay mucha más información sobre Tails en su [propia página web](https://tails.boum.org/doc/index.es.html), así como la típica [relación de preguntas frecuentes](https://tails.boum.org/support/faq/index.es.html).

Si no conocías este sistema operativo, esperamos haberte sido de utilidad!
