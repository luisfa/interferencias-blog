---
layout: post
title:  Hide your face
author: terceranexus6
image:
  feature: banners/header.jpg
tags: privacidad python experimento
---

<img src="{{ site.url }}/assets/images/dev.to/191025/00.jpg" style="display: block; margin: 0 auto;">

I had a brief conversation with a friend about **face recognition**, which made me curious about the state of the art. I got interested in this four or five years ago because I saw make-up for avoiding face recognition and I tried to do it, but it didn't worked. I decided to take back the experiment now and it has been interesting. I'm using what I've learned also to design a security challenge for a group of girls studying cybersecurity and for my Mastodon followers.

This time I decided to follow a methodology more exhaustive than just painting my face as the models I watched (I did that, years ago). I wanted to know hot face recognition in general works. For that I read a couple of articles ([this one](https://www.rankred.com/face-recognition-algorithms-techniques/) and [this one](https://medium.com/@ageitgey/machine-learning-is-fun-part-4-modern-face-recognition-with-deep-learning-c3cffc121d78) are the best I found for my purpose), and experimented with a library called `face_recognition` for python. This was a lot of fun, it can determine if there's a face in a picture or a video and tag the person if recognised. I tried to take stock pictures of a family, tag the persons and see if they are recognised in a different picture.

(*the pictures are done by [Sharon McCutcheon](https://unsplash.com/@sharonmccutcheon) on Unsplash and they don't require the mention but they are awesome, thanks*)

<img src="{{ site.url }}/assets/images/dev.to/191025/01.gif" style="display: block; margin: 0 auto;">

Tagged persons in an image:

<img src="{{ site.url }}/assets/images/dev.to/191025/02.jpeg" style="display: block; margin: 0 auto;">

After that, I made a script to check different images of a model with sketches above, and I noted down if the pattern affected the face. I firstly went with clear negatives and positives and then slowly approached to the limits in between both of them. If was a lot of fun, also requires a bit of imagination and creativity. The idea was to design something easy to draw on a face with common make-up.

Template basic face:

<img src="{{ site.url }}/assets/images/dev.to/191025/03.jpg" style="display: block; margin: 0 auto;">

Obvious negative based on the template:

<img src="{{ site.url }}/assets/images/dev.to/191025/04.jpg" style="display: block; margin: 0 auto;">

Successful sketch:

<img src="{{ site.url }}/assets/images/dev.to/191025/05.jpg" style="display: block; margin: 0 auto;">

Serious design:

<img src="{{ site.url }}/assets/images/dev.to/191025/06.jpg" style="display: block; margin: 0 auto;">

Now, after having a bunch of successful designs, the funniest part: doing the make up. I decided to try on my own, and I realized a trick to guide me: most of the phone and tablet cameras already recognizes the face, so you can start doing some of the designs and then improving them on the go bypassing the camera face recognition. Surprisingly these designs did work both on the python library and the camera face recognition, but on the other hand it's hideous. It's also nice to play with the hair but I'm quite bad at both hair-doing and make-up so...

<img src="{{ site.url }}/assets/images/dev.to/191025/07.jpg" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/dev.to/191025/08.jpg" style="display: block; margin: 0 auto;">

So, I'd like to create something that automatically create those designs but I'm not very good at ML or anything, so for now I'll trust on my creativity and... lack of embarrassment I guess.

As this was a fun project, privacy is quite important and face recognition is becoming a serious matter for privacy in many places, as well as social networks are saving more and more pictures and data about ourselves. Let's consider this point! in the mean time, keep painting your face :p

*Also written in: [https://dev.to/terceranexus6/hide-your-face-mhb](https://dev.to/terceranexus6/hide-your-face-mhb)*
