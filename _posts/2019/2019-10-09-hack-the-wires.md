---
layout: post
title: Hack the wires
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad hardware
---

The maker community is a lot of fun. There's almost a project for any crazy idea you may have, and usually a full tutorial on how to build it. I've loved crazy hardware ideas for a while now, and experimented with different kind of boards and sensors. Today I'm bringing here some interesting security related project I tried or that I'm willing to try and what do we need to make them.

<img src="{{ site.url }}/assets/images/dev.to/htw_1.gif" style="display: block; margin: 0 auto;">

The network probe is one of the most classic projects for security gear. Mine is build over a RPI 3 and an HDMI screen with raspbian and a couple of tools. It's a lot of fun because it's very customizable and can be adapted for many kind of security projects. For making it easier to handle I also added a lithium battery, and all together fits in a pocket. I think that actually one of the more difficult things is to adjust the screen touch options, I actually ended up using a foldable keyboard because ugh. So for now we have a RPI3+Lithium battery+HDMI mini screen+foldable keyboard. We have plenty of choices for configuring scripts, there are tons of github/gitlab projects.

Recently I was linked to a lovable project I'm so looking forward to build, which also uses a PI, but in this case the Zero model. The [Pwnagotchi](https://github.com/evilsocket/pwnagotchi) is your cute companion for Wi-fi handshakes. As described by its programmer "is an A2C-based "AI" leveraging bettercap that learns from its surrounding Wi-Fi environment in order to maximize the crackable WPA key material it captures". A total cutie as well as a nice hardware hacking companion.

<img src="{{ site.url }}/assets/images/dev.to/pwnagotchi.png" style="display: block; margin: 0 auto;">

One of the most classics (and easier comparing to the ones before) is a home-made rubber ducky using an [Attiny board](https://abhijith.live/build-cheaper-version-of-rubber-ducky-using-digispark-attiny85/). A rubber ducky is a pendrive that acts as an human keyboard input on its own. It could launch an attack while you are distracting someone, for example. It became popular thanks to Mr.Robot show, but it actually could be useful in some real situations. In a short speech I gave about the original rubber ducky (sold by Hak5) someone asked me "yeah but could it be used for you know, something which is not bad" and I couldn't think of anything, but what's the point anyway. Anyway Hak5 version is SOOO EXPENSIVE also you can totally make a functional version with Arduino. Maybe also check [this cactus](https://www.tindie.com/products/aprbrother/cactus-whid-wifi-hid-injector-usb-rubberducky/)? Or [this adapter](https://www.adafruit.com/product/2910) that might get in handy when speaking about using USB stuff.

Speaking about Hak5 one of the new products they have is the [Packet Squirrel](https://shop.hak5.org/products/packet-squirrel). It's an Ethernet multi-tool for MITM (man in th emiddle) attacks, it could be also done using a PI3 or PI4 with [Raspbian Buster](https://www.raspberrypi.org/downloads/raspbian/) and **systemd-networkd** as explained [here](https://raspberrypi.stackexchange.com/questions/100515/building-a-packet-squirrel-using-raspberry-pi).

<img src="{{ site.url }}/assets/images/dev.to/htw_2.gif" style="display: block; margin: 0 auto;">

Keeping the mainstream, RPI can be used to create a [Wi-Fi jammer](https://dephace.com/how-to-make-a-wifi-jammer-with-raspberry-pi-3/) that would allow us to mess up with deauthentication BUT in case you want to change the boards you can also try to build an ESP8266 deauther. Check [this](https://github.com/spacehuhn/esp8266_deauther) out, I'm currently trying this one.

Aaaah but let's not forget about other things apart from Wifi, what about RFID stuff? Chamaleon smartcard emulator and Proxmark 3 are quite famous but you can also try basic RFID writing and reading with an arduino (whichever you like the most, UNO, nano, micro...) and the RFID module. The pro of using Chamaleon or Proxmark is the reliable source for RFID tricks in REDTEAM operations. Speaking of which I'm very into this [ring](https://thepihut.com/products/adafruit-rfid-nfc-smart-ring-size-12-ntag213).

I think this is pretty much the list I have in mind, although there are many projects depending on the interests.

*Also written in: [https://dev.to/terceranexus6/hack-the-wires-580p](https://dev.to/terceranexus6/hack-the-wires-580p)*
