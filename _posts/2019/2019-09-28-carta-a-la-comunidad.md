---
layout: post
title: El peso de la electrónica
author: interferencias
image:
  feature: banners/header.jpg
tags: opinión electronica china
---

Es complicado ser consecuente con la tecnología. Como ocurre con tantas cosas de la rutina cotidiana, existen muchos frentes abiertos en la lucha por una sociedad más equitativa. Cuando se trata de tecnología, la ya invisible realidad que nos incomoda pensar se vuelve más fácil de ignorar: ¿de dónde vienen los principales componentes de nuestros aparatos electrónicos¹? 

Esta [noticia](https://www.lainformacion.com/mundo/foxconn-la-fabrica-de-apple-en-la-que-los-trabajadores-se-suicidan_oidt8CTkyBlc0HDAhTYL15/) puede ilustrar un pellizco del tema que quiero abarcar.

La situación tras de la producción electrónica de todo lo que usamos no sólo es precaria; es inhumana. Frente a esto, mi primera reacción ha sido parálisis. No sólo somos parte de la cadena consumidora que alimenta la mano de obra maltratada de la electrónica, también algunos nos identificamos con la industria informática que lo sustenta. Cabe pensar hasta qué punto es necesaria la producción actual de tecnología, más aún conociendo su origen². Me encuentro en una encrucijada, y supongo que como yo hay muchos usuarios y técnicos que se chocan con la realidad de su campo. Leía hace poco³ que un posible motivo de que se haga oídos sordos a esta realidad sea la falta de recursos individuales que un usuario tiene para luchar frente a semejante situación, algo que me resulta familiar con muchos otros temas como la reivindicación por la vida digna de otro animales, el cambio climático o la industria textil (entre otras muchas cosas), causas que también han sido explotadas como un producto más. 

¿Qué haces cuando tu vida, personal y laboral está sustentada con la esclavitud de otros? La pregunta, sacada de contexto, encaja en otras muchas áreas. Estamos en una situación límite insostenible en muchos frentes. Si bien, de nuevo, quiero centrarme en este caso concreto. Desde hace tiempo que intento rebatir la idea de que la tecnología tiene una ética asociada, que pueda hablarse de lo moral o lo inmoral que sea, pero al pensar más allá de las redes y del software, pensar en la electrónica que lo hace posible, me ha hecho dudar. Estamos abarcando problemas tales como el acceso libre y horizontal de Internet, la no censura, el anonimato o la comunicación cifrada (quiero aclarar que los considero fundamentales y que requieren activismo inmediato) ignorando que toda aplicación de esos derechos requiere un análisis más profundo, una investigación de su lugar en el mundo. Quizás sea imposible llegar a plantear soluciones de uno sin lo otro, pues una tecnología horizontal que no sea sostenible se cimienta en una base tambaleante, dependiente de un sistema abusivo. Si la comunidad ha sido capaz de actuar sin paralizarse frente a la centralización y la privatización de sistemas, quiero confiar en que será capaz de mascar la autocrítica, de no estancarse en la comodidad de “somos los buenos porque hay malos más malos” y dará un paso en esta lucha tan complicada cuando otros sentimos vértigo al mirar a ese acantilado.  

1] _Tales como móviles, ordenadores, consolas, dispositivos de Microsoft, Google, Apple, Nintendo y muchos más nombres familiares._
2] _Entre otros, Foxxcom, una de las principales empresas distribuidoras y productoras de electrónica y componentes de China._ 
3] _La máquina es tu amo y señor – Yang, Jenny Chan, Xu Lizhi, Li Fei y Zhang Xiaoqio_