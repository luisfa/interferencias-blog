---
layout: post
title: Noticias semana 26 de agosto al 1 de septiembre de 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

Estas son las noticias más relevantes que hemos encontrado en nuestras redes sociales esta semana:

- **"Amazon’s Ring barred cops from using 'surveillance' to describe its products"**:
[https://gizmodo.com/ring-barred-cops-from-using-surveillance-to-describe-it-1837380102](https://gizmodo.com/ring-barred-cops-from-using-surveillance-to-describe-it-1837380102)
- **"Facebook really doesn’t want you to read these emails"**:
[https://techcrunch.com/2019/08/23/facebook-really-doesnt-want-you-to-read-these-emails/](https://techcrunch.com/2019/08/23/facebook-really-doesnt-want-you-to-read-these-emails/)
- **"Robot-lución: el gran reto de gobernar y convivir con las máquinas"**:
[https://elpais.com/elpais/2019/08/23/ideas/1566551575_254488.html](https://elpais.com/elpais/2019/08/23/ideas/1566551575_254488.html)
- **"Adam Alter: 'La adicción a las pantallas avanza silenciosa'"**:
[https://elpais.com/tecnologia/2018/04/24/actualidad/1524577831_486816.html](https://elpais.com/tecnologia/2018/04/24/actualidad/1524577831_486816.html)
- **"¿Te están espiando el móvil? Así puedes descubrirlo"**:
[https://elpais.com/tecnologia/2019/08/23/actualidad/1566579318_812378.html](https://elpais.com/tecnologia/2019/08/23/actualidad/1566579318_812378.html)
- **"EU governments choose independence from US cloud providers with Nextcloud"**:
[https://nextcloud.com/blog/eu-governments-choose-independence-from-us-cloud-providers-with-nextcloud/](https://nextcloud.com/blog/eu-governments-choose-independence-from-us-cloud-providers-with-nextcloud/)
- **"Privacy Fundamentalism"**:
[https://stratechery.com/2019/privacy-fundamentalism/](https://stratechery.com/2019/privacy-fundamentalism/)
- **"Silicon Valley amenaza con imitar al polémico sistema de comportamiento social chino"**:
[https://www.eleconomista.es/tecnologia/noticias/10056661/08/19/Silicon-Valley-amenaza-con-imitar-al-polemico-sistema-de-comportamiento-social-chino.html](https://www.eleconomista.es/tecnologia/noticias/10056661/08/19/Silicon-Valley-amenaza-con-imitar-al-polemico-sistema-de-comportamiento-social-chino.html)
- **"Alibaba, Tencent, five others to receive first chinese government cryptocurrency"**:
[https://www.forbes.com/sites/michaeldelcastillo/2019/08/27/alibaba-tencent-five-others-to-recieve-first-chinese-government-cryptocurrency/](https://www.forbes.com/sites/michaeldelcastillo/2019/08/27/alibaba-tencent-five-others-to-recieve-first-chinese-government-cryptocurrency/)
- **"New Android warning: 100M+ users installed app with nasty malware inside"**:
[https://www.forbes.com/sites/zakdoffman/2019/08/27/android-warning-nasty-malware-hiding-inside-app-installed-by-100m-users/](https://www.forbes.com/sites/zakdoffman/2019/08/27/android-warning-nasty-malware-hiding-inside-app-installed-by-100m-users/)
- **"Harvard student says he was barred from U.S. over his friends’ social media posts**:
[https://www.nytimes.com/2019/08/27/us/harvard-student-ismail-ajjawi.html](https://www.nytimes.com/2019/08/27/us/harvard-student-ismail-ajjawi.html)
- **"Feds ordered Google location dragnet to solve Wisconsin bank robbery**:
[https://www.theverge.com/2019/8/28/20836855/reverse-location-search-warrant-dragnet-bank-robbery-fbi](https://www.theverge.com/2019/8/28/20836855/reverse-location-search-warrant-dragnet-bank-robbery-fbi)
- **"Facebook tiene perfilados a 20 millones de españoles con etiquetas sensibles, como ideología u orientación sexual**:
[https://eldiario.es/tecnologia/Facebook-etiquetados-espanoles-ideologia-preferencias_0_936156575.html](https://eldiario.es/tecnologia/Facebook-etiquetados-espanoles-ideologia-preferencias_0_936156575.html)
- **"Con Ring de Amazon la policía de EEUU está construyendo el mayor sistema de vigilancia dentro y fuera de casa de sus usuarios**:
[https://magnet.xataka.com/en-diez-minutos/policia-no-necesita-orden-para-espiar-tu-casa-ahora-puede-hacerlo-gracias-a-amazon](https://magnet.xataka.com/en-diez-minutos/policia-no-necesita-orden-para-espiar-tu-casa-ahora-puede-hacerlo-gracias-a-amazon)
- **"Llega el reconocimiento facial a las escuelas... y las multas por 'espiar' a estudiantes**:
[https://www.elconfidencial.com/tecnologia/2019-08-29/reconocimiento-facial-multa-gdpr-suecia_2197399/](https://www.elconfidencial.com/tecnologia/2019-08-29/reconocimiento-facial-multa-gdpr-suecia_2197399/)
- **"YouTube said to be fined up to $200 million for children’s privacy violations**:
[https://www.nytimes.com/2019/08/30/technology/youtube-childrens-privacy-fine.html](https://www.nytimes.com/2019/08/30/technology/youtube-childrens-privacy-fine.html)
- **"Websites have been quietly hacking iPhones for years, says Google"**:
[https://www.technologyreview.com/s/614243/websites-have-been-quietly-hacking-iphones-for-years-says-google/](https://www.technologyreview.com/s/614243/websites-have-been-quietly-hacking-iphones-for-years-says-google/)
- **"La cuenta de Jack Dorsey, CEO de Twitter, ha sido hackeada... en Twitter"**:
[https://www.xataka.com/servicios/cuenta-jack-dorsey-ceo-twitter-ha-sido-hackeada-twitter](https://www.xataka.com/servicios/cuenta-jack-dorsey-ceo-twitter-ha-sido-hackeada-twitter)
- **"Messaging app Telegram moves to protect identity of Hong Kong protesters"**:
[https://www.reuters.com/article/us-hongkong-telegram-exclusive/exclusive-messaging-app-telegram-moves-to-protect-identity-of-hong-kong-protesters-idUSKCN1VK2NI](https://www.reuters.com/article/us-hongkong-telegram-exclusive/exclusive-messaging-app-telegram-moves-to-protect-identity-of-hong-kong-protesters-idUSKCN1VK2NI)

### Redes sociales:

- **_(Hilo)_ "¿Qué pasa cuando inicias Google Chrome por primera vez en una máquina con Windows 10?"**:
[https://twitter.com/jonathansampson/status/1165493206441779200](https://twitter.com/jonathansampson/status/1165493206441779200)

- **_(Hilo)_ "Facebook escanea las librerías del sistema desde el teléfono del usuario de la aplicación de Android en segundo plano y las carga en su servidor"**:
[https://twitter.com/wongmjane/status/1167463054709334017](https://twitter.com/wongmjane/status/1167463054709334017)

Recuerda que además de poder seguirnos en [Twitter](https://twitter.com/Inter_ferencias) y [Mastodon](https://mastodon.technology/@interferencias), te invitamos a participar y comentar sobre todo esto en nuestro grupo de [Telegram](https://t.me/inter_ferencias).
