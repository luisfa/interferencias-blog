---
layout: post
title: Noticias semana 15 al 21 de julio de 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: noticias
---

Estas son las noticias más relevantes que hemos encontrado en nuestras redes sociales esta semana:

- **Ciberseguridad: nuevo enclave para la seguridad internacional**: [https://polikracia.com/ciberseguridad-internacional-wannacry-infraestructuras/](https://polikracia.com/ciberseguridad-internacional-wannacry-infraestructuras/)
- **NHS teams up with Amazon to bring Alexa to patients**: [https://www.theguardian.com/society/2019/jul/10/nhs-teams-up-with-amazon-to-bring-alexa-to-patients](https://www.theguardian.com/society/2019/jul/10/nhs-teams-up-with-amazon-to-bring-alexa-to-patients)
- **Voice-powered virtual assistant Alexa saves girlfriend from a ‘beating’**: [https://www.news.com.au/technology/gadgets/voicepowered-virtual-assistant-alexa-saves-girlfriend-from-a-beating/news-story/3d7e8cdf045163e2d09a52c269e8e28a](https://www.news.com.au/technology/gadgets/voicepowered-virtual-assistant-alexa-saves-girlfriend-from-a-beating/news-story/3d7e8cdf045163e2d09a52c269e8e28a)
- **Facial Recognition Tech Is Growing Stronger, Thanks to Your Face**: [https://www.nytimes.com/2019/07/13/technology/databases-faces-facial-recognition-technology.html](https://www.nytimes.com/2019/07/13/technology/databases-faces-facial-recognition-technology.html)
- **Vulnerabilidad en #WhatsApp y #Telegram: Media File Jacking**: [https://blog.segu-info.com.ar/2019/07/vulnerabilidad-en-whatsapp-y-telegram.html](https://blog.segu-info.com.ar/2019/07/vulnerabilidad-en-whatsapp-y-telegram.html)
- **Decirle a Amazon que exija políticas de privacidad para los dispositivos con conexión a Internet**: [https://foundation.mozilla.org/es/campaigns/tell-amazon-connected-devices-must-have-privacy-policies/](https://foundation.mozilla.org/es/campaigns/tell-amazon-connected-devices-must-have-privacy-policies/)
- **The PGP Problem**: [https://latacora.micro.blog/2019/07/16/the-pgp-problem.html](https://latacora.micro.blog/2019/07/16/the-pgp-problem.html)
- **Trump amenaza a Google con investigarla por traición**: [https://www.eleconomista.es/tecnologia/noticias/9997139/07/19/Trump-investigara-a-Google-tras-las-acusaciones-de-Thiel-de-colaboracionismo-con-China.html](https://www.eleconomista.es/tecnologia/noticias/9997139/07/19/Trump-investigara-a-Google-tras-las-acusaciones-de-Thiel-de-colaboracionismo-con-China.html)
- **Facebook detiene el lanzamiento de Libra hasta solucionar las dudas regulatorias**: [https://www.genbeta.com/actualidad/facebook-detiene-lanzamiento-libra-solucionar-dudas-regulatorias](https://www.genbeta.com/actualidad/facebook-detiene-lanzamiento-libra-solucionar-dudas-regulatorias)
- **Son pocos los directivos que confían en sus estrategias de ciberseguridad**: [http://www.computing.es/seguridad/informes/1113229002501/pocos-directivos-confian-estrategias-de-ciberseguridad.1.html](http://www.computing.es/seguridad/informes/1113229002501/pocos-directivos-confian-estrategias-de-ciberseguridad.1.html)
- **New Attack Lets Android Apps Capture Loudspeaker Data Without Any Permission**: [https://thehackernews.com/2019/07/android-side-channel-attacks.html](https://thehackernews.com/2019/07/android-side-channel-attacks.html)
- **Victory: Oakland City Council Votes to Ban Government Use of Face Surveillance**: [https://www.eff.org/deeplinks/2019/07/victory-oakland-city-council-votes-ban-government-use-face-surveillance](https://www.eff.org/deeplinks/2019/07/victory-oakland-city-council-votes-ban-government-use-face-surveillance)
- **Fingerprinting – Con WebRTC IP local y Media IDs**: [https://mierda.tv/2019/07/19/fingerprinting-con-webrtc-ip-local-y-media-ids/](https://mierda.tv/2019/07/19/fingerprinting-con-webrtc-ip-local-y-media-ids/)
- **El robo del siglo en Bulgaria: un veinteañero 'hackea' a (casi) todo el país**: [https://www.elconfidencial.com/tecnologia/2019-07-20/bulgaria-secuestro-datos-hackers-delincuente-hacienda_2131963/](https://www.elconfidencial.com/tecnologia/2019-07-20/bulgaria-secuestro-datos-hackers-delincuente-hacienda_2131963/)

Recuerda que además de poder seguirnos en [Twitter](https://twitter.com/Inter_ferencias) y [Mastodon](https://mastodon.technology/@interferencias), te invitamos a participar y comentar sobre todo esto en nuestro grupo de [Telegram](https://t.me/inter_ferencias).
