---
layout: post
title: Publicados los vídeos de esLibre 2020
author: germaaan
image:
  feature: banners/header.jpg
tags: eventos privacidad seguridad
---

<img src="{{ site.url }}/assets/images/eslibre/eslibre_2020.jpg" style="display: block; margin: 0 auto;">

<p>
Ya os comentamos que este año dada la situación actual no pudimos celebrar las <strong>JASYP</strong>, pero eso no impidió organizar algo en su lugar. Aprovechando que la edición de este año de <strong>es<span style="color:red">Libre</span></strong> se realizó de forma online, organizamos una sala sobre <strong>Derechos Digitales, Privacidad en Internet y Seguridad Informática</strong>.
</p>

<p>
Todos los detalles ya los explicamos en su página correspondiente, así que simplemente os dejamos un enlace allí para que podáis echarle un ojo.
</p>

<h3>➡️ <a href="{{ site.url }}/eslibre2020">Sala Derechos Digitales, Privacidad en Internet y Seguridad Informática</a></h3>
