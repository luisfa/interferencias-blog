---
layout: post
title: Attacks in times of fear
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad phishing covid19
---

<img src="{{ site.url }}/assets/images/dev.to/200317/9r1t0d99bkb673e9wkbv.webp" style="display: block; margin: 0 auto;">

Part of my job involves creating phishing attacks to aware companies about dangerous emails and links. One of the easiest way to distract from the fact that it's false is through fear: using the word "URGENT", "NOW", "I NEED IT REALLY FAST", and using **bold** words. These things sometimes trigger workers to act as soon as they can, without carefully looking at the information in the email or checking if it's legit. This all worldwide situation of health alarm is making these attacks easier to perform, but they are not hired, they are for real.

<img src="{{ site.url }}/assets/images/dev.to/200317/es62ay901e6aj48ejhve.jpeg" style="display: block; margin: 0 auto;">

It's difficult for me to imagine what kind of person would take advantage of worldwide panic to attack Hospital systems with ransonwares or make phishing emails. It's disgusting, but in any case today I want to tell you some advices to tell if an email is legit or not. Please aware your family and friends.

* Check the sender carefully. It sometimes seems to be a legit source but it actually changes something: the domain, a single letter, a symbol (Russian alphabet or similar instead of regular Latin ascii).
* If you are unsure, DON'T interact or reply or download the content. Simply don't. Search on the internet about a scam with the subject of the email, maybe, but DO NOT interact. There are certain attacks that hide viruses and scripts in excel or documents (macros) to hide from the antivirus and then infects your system. Don't!!
* Sometimes this kind of emails attach infected links. These days (everyday but mostly these days) before clicking any link, see the content. [Unwrap it if it's a short link](https://www.checkshorturl.com/) or safely paste it if it's attached in a word.
* If a COVID-19 webapp is asking you too many personal questions (your exact address, bank account information, passwords) don't use it, don't share it. Never. Pay attention to your local legit information. If a smartphone applications need way more permissions that needed (contacts, systems, camera...) don't use or download it.

<img src="{{ site.url }}/assets/images/dev.to/200317/0akr056gfl4tz41jwdor.jpeg" style="display: block; margin: 0 auto;">

* Don't trust open WiFis in your neighbourhood with names that would suggest they are publicly legit if you haven't heard of it. It could be someone using a [WiFi pineapple](https://shop.hak5.org/products/wifi-pineapple) to get your information.

**I'm open to answer to questions here if you are unsure about your security** these days. Please don't attach suspicious links in your answers, better describe the situation. Stay safe.

*Also written in: [https://dev.to/terceranexus6/attacks-in-times-of-fear-4a0f](https://dev.to/terceranexus6/attacks-in-times-of-fear-4a0f)*
