---
layout: post
title: I know what you did
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad osint python
---

<img src="{{ site.url }}/assets/images/dev.to/200316/m20gm429hiaf2c23jpyx.webp" style="display: block; margin: 0 auto;">

An important part of offensive security is basically knowing **what** to attack. Sometimes you have to face a really big project and before launching any kind of attack, you must learn the weaknesses and as much as information about your target as you could gather. Good for us, there are tons of useful tools and resources for this.

When trying to get subdomains of a target, we have for example Subl1st3r. Gathering subdomains is important because you might find a forgotten subdomain with unpatched bugs that can lead us to bigger issues, data leaks and more. [Subl1st3r](https://github.com/aboul3la/Sublist3r) is an open Linux terminal, for example using `python3 sublist3r.py -d mydomain.com`. Another useful tool is [knockpy](https://github.com/guelfoweb/knock), it's slow but it gathers tons of subdomains, this is a nice tool even though it doesn't use Python 3, that's why I would add here [Photon](https://github.com/s0md3v/Photon), which does and is similar to knockpy, plus is faster.

<img src="{{ site.url }}/assets/images/dev.to/200316/z9lm61rgyp41ge4d2bh8.png" style="display: block; margin: 0 auto;">

What I like the most about this last one is that it creates a directory with all the information. So nice and tidy!

<img src="{{ site.url }}/assets/images/dev.to/200316/giphy.gif" style="display: block; margin: 0 auto;">

Let's not forget about some famous tools such as: [Shodan](https://www.shodan.io/), [Censys](https://censys.io/) or [Hunter.io](https://hunter.io/). These are nice, some of them allow you to have an API key in order to build tools with them.

<img src="{{ site.url }}/assets/images/dev.to/200316/b1dwdcwgvwdj4stzifch.jpeg" style="display: block; margin: 0 auto;">

There are some frameworks for making these **❀⋆❀ even tidier ❀⋆❀**! For example [Spiderfoot](https://www.spiderfoot.net/). After trying to set up other frameworks, the first time I faced Spiderfoot I was expecting tons of bug handling and issues but it was quite easy, just `git clone` it, `pip3 install -r requirements.txt` and execute! Easy pie. This tool is interesting because apart from launching tons of tools (some of them need an API key for gathering information) it generates graphs, which is very useful. And by the way it's so pretty well documented so if you are new with this, you will thank them.

Learning this, you might want consider cleaning up your webpage in order not to let anything spreading information about it that should not be there!

*Also written in: [https://dev.to/terceranexus6/i-know-what-you-did-4dk](https://dev.to/terceranexus6/i-know-what-you-did-4dk)*
