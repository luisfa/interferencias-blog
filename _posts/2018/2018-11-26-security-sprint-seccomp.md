---
layout: post
title: Security Sprint&#58 Using Seccomp for secure containers
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad contenedores docker
---

Last week I skipped Security Sprint, oops! But today I'm bringing you another **DevSecOps** content. In this case we are learning about Seccomp and how to apply it for secure containers. Secure computing mode (seccomp) is a Linux kernel feature, as explained in the [Docker documentation](https://docs.docker.com/engine/security/seccomp/).

You can use this feature to restrict your application’s access, and making its security stronger. They're defined in a JSON file that is applied when a container starts. This is only available if **Docker** has been built with seccomp and the kernel is configured with `CONFIG_SECCOMP` enabled. To check if our kernel supports seccomp, we should write:

```
$ grep CONFIG_SECCOMP= /boot/config-$(uname -r)
```

If it returns `CONFIG_SECCOMP=y`, it does support it. Now, let's see an example of a json which have been defined with seccomp permissions to disable allowing containers to run seccomp.

```
{
  "defaultAction": "SCMP_ACT_ALLOW",
  "architectures": [
    "SCMP_ARCH_X86_64",
    "SCMP_ARCH_X86",
    "SCMP_ARCH_X32"
  ],
  "syscalls": [
    {
      "name": "chmod",
      "action": "SCMP_ACT_ERRNO",
      "args": []
    },
    {
      "name": "chown",
      "action": "SCMP_ACT_ERRNO",
      "args": []
    },
    {
      "name": "chown32",
      "action": "SCMP_ACT_ERRNO",
      "args": []
    }
  ]
}
```

If we launch our container with docker (`docker run ...`) and try `chmod` we would receive a permission error. This allows us to prevent from attacks or prevent vulnerabilities in different versions in case we don't want to change our application. Be careful, tho. Seccomp works with syscalls, and for that we need to know which ones are being used, depends on the system. For example, is not the same to create rules for a container based in Alpine (uses `strace`) than a container based in Ubuntu (uses `strace-ubuntu`). There's an example of all of this in [Katacoda](https://www.katacoda.com/courses/docker-security/intro-to-seccomp).

<img src="{{ site.url }}/assets/images/dev.to/Screenshot-from-2016-02-10-10-33-59.png.jpeg" style="display: block; margin: 0 auto;">

The default profile already blocks some `syscalls`, [here's a list of them](https://docs.docker.com/engine/security/seccomp/#significant-syscalls-blocked-by-the-default-profile). But of course we get to choose either the default or a customized version, depending on our needs.

Personally I think a good option for fast security setup in your container would be a vuln scanning and performing a list of blocked syscall regarding those version vuln until you are able to change the version (or this version is being patched, in case of zero days). For scanning, I suggest using CoreOS, an open source project hosted by -gasp- red hat. If we have a populated data base, we can start Clair service using `docker-compose up -d clair` command and send it Docker Images to scan and return which vulnerabilities it contains. For this, we are using [klar](https://github.com/optiopay/klar/releases/download/v1.5/klar-1.5-linux-amd64), [here's the github repo](https://github.com/optiopay/klar/), klar is an Integration of Clair and Docker Registry. For jsonize our data we can use [jq](https://stedolan.github.io/jq/). An example, now, of a command line using all of these would be:

```
CLAIR_ADDR=https://our_service CLAIR_OUTPUT=High CLAIR_THRESHOLD=15 JSON_OUTPUT=true klar postgres:latest | jq
```

This last scanning was public, from the public Docker Registry. We can scan a private image, too, using tags. Here's another example of a command line:

```
CLAIR_ADDR=http://our_service \
  CLAIR_OUTPUT=Low CLAIR_THRESHOLD=10 \
  klar our_tag/postgres:latest
```

You can see an example of this [here](https://www.katacoda.com/courses/docker-security/image-scanning-with-clair).

<img src="{{ site.url }}/assets/images/dev.to/Screenshot-from-2016-02-10-10-33-59.png.jpeg" style="display: block; margin: 0 auto;">

Once we have our output (which will contain the vulnerability name, ID, description and others) we can perform our seccomp rule definition, in json too, as already described.

Welp, this is it. I hope you enjoyed this, please feel free to add more tools, ideas and information.

*Also written in: [https://dev.to/terceranexus6/security-sprint-using-seccomp-for-secure-containers--3nk5](https://dev.to/terceranexus6/security-sprint-using-seccomp-for-secure-containers--3nk5)*
