---
layout: post
title:  Navaja Negra 8 - ¿Qué tienen que ver los xenomorfos con la seguridad?
author: germaaan
image:
  feature: banners/header.jpg
tags: charlas
---

**Navaja Negra** es un congreso de seguridad informática que se celebra en la ciudad de Albacete del 4 al 6 de octubre 2018 (de jueves a sábado) y que incluye ponencias, talleres, demostraciones y más actividades relacionadas con el mundo de la seguridad y la tecnología.

Este año, Paula participó con la charla **"¿Qué tienen que ver los xenomorfos con la seguridad?"**:

<blockquote>
<p>
Nuestra forma de interactuar con la tecnología avanza a pasos agigantados. Nuestra interacción natural se torna distribuída, cada vez más dependiente de pequeños dispositivos conectados entre si. En un sistema ubícuo podemos implementar nuevas formas de comunicación e interacción en el que la seguridad no depende sólo de cada nodo por separado, si no que puede dividirse por la red para crear una inteligencia común, “una mente colmena” y unas barreras menos vulnerables. Esta charla pretende introducir el concepto de computación ubícua y cómo diseñar un sistema de detección y prevención de ataques en dicho sistema, incluso con sensores de bajo coste.
</p>
<p>
La investigación, principalmente, se torna alrededor del paper “Computational Intelligence in Wireless Sensor Networks” de Ajith Abraham, en uno de los capítulos se describe la capacidad de detectar ataques a través de algoritmos evolutivos en una red con varios nodos que actúan por separado para un fin común. Sobre esto, intento volcar esta aplicación en hardware probando tanto con nodos básicos como arduino a cosas más complejas como rpi. Las simulaciones pueden realizarse a través de MiXim (basado en OmNet++) o incluso sin necesidad de una representación gráfica, si no creando un simulador que se centre en el contenido de las comunicaciones, utilizando un sistema de contratos parecido al de Ether. Vale la pena echar una mirada a esta posibilidad, pues tiene relación directa con dos grandes picos de la tecnología actual: el consenso descentralizado e Internet de las cosas.
</p>
</blockquote>

<iframe width="560" height="315" src="https://www.youtube.com/embed/GAi5LeHU-3w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
