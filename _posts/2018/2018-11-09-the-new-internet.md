---
layout: post
title: Security Sprint&#58 The new Internet
author: terceranexus6
image:
  feature: banners/header.jpg
tags: ipfs seguridad
---

Hello again! and welcome to the weekly **Security Sprint** :p

Today I'm going to introduce a very exciting thing. But before, let's go back in time for a moment. We are in the late 80's, internet is very exciting, everyone want to have a webpage... Communication has changed, and we are going into a new lifestyle, in which we can be anonymous, there are no walls between users and knowledge is open. But... uh-oh, some countries are not very happy with this statements... Internet begins to have such importance, **it shapes society**. If you where an ambitious company or country, wouldn't you think... Who has internet control has society influence, right?

Step by step, internet as we know it is currently being **limited, censored and monitored**. To prevent and fight against this, there are associations, activists and such... and one of the proposals is... IPFS protocol.

IPFS is, as the official website describes *"peer-to-peer hypermedia protocol
to make the web faster, safer, and more open"*. It wants to offer a distributed alternative protocol, making it more resilient which is handy in case of developing countries, for example. Here's an info-graphic from the official webpage:

<img src="{{ site.url }}/assets/images/dev.to/lqh1tsxch21kwlbjfz4r.png" style="display: block; margin: 0 auto;">

[Installation works in different OS](https://docs.ipfs.io/introduction/install/), but I'm focusing on Linux. The documentation is clear, and it has a script for installing it. After the installation, we should create a repository in which we are storing the data/settings. For this, we are going to use:

```
$ ipfs init
initializing ipfs node at /Users/jbenet/.go-ipfs
generating 2048-bit RSA keypair...done
...
```

Our node ID is the `peer identity` hash. If we've been successful, we should be able to start as told in the repo installation:

```
ipfs cat /ipfs/ourhash/readme
```

Remember to change `ourhash` with our `peer identity` hash. Once we did our basic setup as told in the readme, we are ready to go online...

```
$ ipfs daemon
Initializing daemon...
...
```

Yay! we are in the network... You can see the ipfs address or your peers using `ipfs swarm peers` command. There's also a web interface console, we can access in the link [http://localhost:5001/ipfs/yourhash](http://localhost:5001/ipfs/yourhash).

<img src="{{ site.url }}/assets/images/dev.to/2wi2a4s14ak07lmqdzq4.png" style="display: block; margin: 0 auto;">

And we can add stuff in "Files".

<img src="{{ site.url }}/assets/images/dev.to/pfjnxe7pe0v8dn8c1djc.png" style="display: block; margin: 0 auto;">

The security in IPFS is still *weak*, as they are still claiming auditing. It's a nice opportunity to investigate about distributed technologies!

Hope you guys enjoyed this introduction to IPFS, I'd love to write soon about it again, deeper.

*Also written in: [https://dev.to/terceranexus6/security-sprint-the-new-internet-7fj](https://dev.to/terceranexus6/security-sprint-the-new-internet-7fj)*
